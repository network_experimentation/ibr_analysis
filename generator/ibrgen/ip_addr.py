
class Subnet:
    """
    The subnet class is used to to represent a sequence of
    multiple IP addresses
    """
    def __len__(self):
        return 2**(32 - self.netmask)
    def __getitem__(self, key):
        if type(key) == int:
            if key >= len(self):
                raise StopIteration
            return (self.ip + key)
        elif type(key) == slice:
            start = key.start
            end = key.stop
            increment = 1 if key.step is None else key.step

            return [self[ii] for ii in range(start, end, increment)]

    def __init__(self, str_rep):
        self.netmask = None
        self.ip = None
        if str_rep.find('/') == -1:
            raise Exception("Cannot parse subnetwork %s, no subnet spec"
                            % str_rep)
        elif len(str_rep.split('/')) > 2:
            raise Exception(
                "Cannot parse subnetwork %s, multiple slashes in spec" %
                str_rep)
        else:
            # Note we can raise an exception here if the netmask is gibberish
            self.netmask = int(str_rep.split('/')[1])
            self.ip = IPAddr(str_rep.split('/')[0])

    def __str__(self):
        retval = str(self.ip) + "/" + str(self.netmask)
        return retval


class IPAddr:
    def __len__(self):
        return 1

    def int_ip_to_str(ip_address):
        """
        int_ip_to_str(subject [int]):
        Converts an ip address into a dotted quad string
        representation.  Assumes that the address is valid.
        """

        o1 = (ip_address >> 24) & 0x000000FF
        o2 = (ip_address >> 16) & 0x000000FF
        o3 = (ip_address >> 8) & 0x000000FF
        o4 = ip_address & 0x000000FF
        return "%d.%d.%d.%d" % (o1, o2, o3, o4)

    def str_ip_to_int(subject):
        """
        string_ip_to_int(subject)
        Converts a valid string representation as dotted quads
        to the integer equivalent.
        """
        if IPAddr.validate_str_address(subject):
            values = list(map(int, subject.split('.')))
            result = (values[0] << 24) + (values[1] << 16) + (values[2] << 8) \
                + values[3]
            return result
        else:
            return None

    def validate_int_address(subject):
        """
        Checks an integer IP address is in range (0, 2^32).  True good, false
        bad.
        """
        if subject >= 0 and subject <= 0xFFFFFFFF:
            return True
        else:
            return False

    def validate_str_address(subject):
        """
        Checks a stirng address to see if its acceptable, four integers
        between 0 and 256.  True good, False bad.
        """

        values = subject.split('.')
        # First check -- four dot separated elements
        if len(values) != 4:
            return False

        # Second check -- all values are integer
        try:
            values = map(int, values)
        except:
            return False

        # Third check -- all values are in [0,255]
        if len(list(filter(lambda x:(x >= 0 and x <= 255), values))) != 4:
            return False
        # We've exhausted all the error cases, so return true
        return True

    def __eq__(self, subject):
        """
        __eq__

        Equivalence test; returns true when both values have the same
        self.addr value
        """
        if isinstance(subject, IPAddr):
            return self.addr == subject.addr
        elif isinstance(subject, int):
            return self.addr == subject
        elif isinstance(subject, str):
            return IPAddr.int_ip_to_str(self.addr) == subject

    def __add__(self, subject):
        """
        __add__ (Self, subject)
        Creates a new IPAddr object with the sum of the integer
        address in the result.  Error management will be handled by the
        constructor.
        """
        if isinstance(subject, int):
            return IPAddr(self.addr + subject)
        elif isinstance(subject, IPAddr):
            return IPAddr(self.addr + subject.addr)

    def __str__(self):
        """
        String representation of the IP address; this will be in
        dotted quad format.
        """
        return IPAddr.int_ip_to_str(self.addr)


    def __repr__(self):
        """
        Integer representation of address
        """
        return str(self.addr)


    def __init__(self, subject):
        """
        __init__(self, subject [int | str])
        Constructs the address object, storing the
        subject in the internal value addr.  Handles integers
        or strings transparently.
        """
        if isinstance(subject, str):
            if IPAddr.validate_str_address(subject):
                self.addr = IPAddr.str_ip_to_int(subject)
            else:
                raise Exception("Invalid String IP Address", subject)
        elif isinstance(subject, int):
            if IPAddr.validate_int_address(subject):
                self.addr = subject
            else:
                raise Exception("Invalid Integer IP Address", subject)
        else:
            raise Exception("Unknown Type of IP Address", subject)
