import json
import logging
import multiprocessing as mp
import os
import yaml

import ibrgen.ip_addr as ip
import ibrgen.subnet_scanner as ss


class ScanManager:
    """ Main class to kick off the individual SubnetScanners; each scsanner will
        run in its own sub-process.
    """

    def __init__(self, config_file_path):
        """ Constuctor.

        :param str config_file_path: Path to the configuration file; see
                                     etc/config2.yml for an example.
        """
        self.started = False
        self.config_file_path = config_file_path
        self.process_list = []
        self.stop_flag = mp.Value('i', 0)

        with open(config_file_path, 'r') as fp:
            config_dict = yaml.load(fp, Loader=yaml.FullLoader)
        logging.debug('ScanManager loaded config file %s:\n%s\n' % (
            config_file_path, json.dumps(config_dict, indent=4)))

        # Most of the detail about the traffic we generate is stored in one
        # or more "profile" files (eg, etc/profiles/random_scans.profile.yml).
        # So load the list of .profile.yml files that we'll use.
        self.profile_paths = self.__get_profile_file_paths(config_dict)

        self.subnets_to_scan = []
        self.__init_subnets(config_dict)
        return

    def __init_subnets(self, config_dict):
        subnet_str_list = config_dict['subnets_to_scan']
        for subnet_str in subnet_str_list:
            subnet = ip.Subnet(subnet_str)
            self.subnets_to_scan.append(subnet)
        return

    def start_scans(self):
        # Sanity check...
        if self.started:
            raise Exception("Error: Scans have been started already!")
        self.started = True

        # For each profile, for each subnet being scanned, create a subnet
        # scanner and run it in its own process.
        for profile_path in self.profile_paths:
            for subnet in self.subnets_to_scan:
                subnet_str = str(subnet)
                args=(subnet_str, self.config_file_path, profile_path,
                    self.stop_flag)
                process = mp.Process(target=ss.run_scanner, args=args,
                    daemon=True)
                self.process_list.append(process)
                process.start()

        return

    def stop_scans(self):
        """ Stop all of the scanning sub-process and wait for them to terminate.
        """
        self.stop_flag.value = 1
        for process in self.process_list:
            process.join()
        return

    def __get_profile_file_paths(self, config_dict):
        """ Get the list of .profile.yml files that describe the "profiles" of
            scanning traffic we want to use.
        
        :returns: A list of file paths to .profile.yml files.
        :rtype: list<str>
        """
        dir_path = config_dict["profiles_dir"]
        if not os.path.exists(dir_path):
            raise ValueError("Error: The specified profiles directory does "
                "not exist!  Directory not found: %s" % dir_path)

        retval = []
        files = os.listdir(dir_path)
        for filename in files:
            if filename.endswith(".profile.yml"):
                file_path = os.path.join(dir_path, filename)
                retval.append(file_path)
                logging.info("ScanManager found the following profile: %s" %
                    file_path)

        if not retval:
            raise ValueError("Error: Did not find any .profile.yml files in "
                "the following directory: %s" % dir_path)
        return retval
