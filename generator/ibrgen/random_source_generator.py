import random

import ibrgen.random_ip_addr_generator as ripag
import ibrgen.random_port_generator as rpg


class WeightedSourceIpAndPortGenerator:
    """ A triple of (random IP generator, random port generator, weight)

    This class represents three values tied together:
      - A random IP address generator (used for generating source IP addr)
      - A random port generator (used for generating random source ports)
      - A weight, which is used to indicate how often this particular 
        (source IP, source port) generator pair should be used, compared to the
        other (source IP, source port) generators that are also defined.
    """

    def __init__(self, config_dict, low_bound):
        """ Constructor

        config_dict should look something like this:
        {
            "description" : "Less-commonly seen Shodan IP",
            "strategy"    : "static_address",
            "address"     : "85.25.103.50",
            "source_port_config": [
                {
                    "strategy" : "uniform_dist",
                    "args" :
                    {
                        "min" : 32768,
                        "max" : 60999
                    }
                }
            ],
            "weight" : 10
        }

        ...or something like this:
        {
            "strategy"    : "random_address",
            "subnet_range": "162.142.125.0/24",
            "source_port_config": [
                {
                    "strategy" : "random_port"
                }
            ]
        },

        ...etc...  See the comments for the various "strategies" in 
        random_ip_addr_generator.py for details on properties related to 
        generating IP addresses, and see the coments in 
        random_port_generator.py for properties related to randomly-generating
        port values.

        Note that if a "weight" property is not specified in config_dict, the
        weight will be defaulted to 1.
        """
        self.addr_generator = ripag.RandomIPAddrGenerator(config_dict)
        self.port_generator = rpg.RandomPortGenerator(
            config_dict['source_port_configs'])
        self.weight = int(config_dict.get('weight', 1))
        if self.weight < 1:
            raise ValueError("Error: The following is not a valid weight: %d.  "
                "Weights must be >= 1!" % self.weight)

        self.low_bound = low_bound
        self.high_bound = self.low_bound + self.weight
        return

    def in_bounds(self, value):
        """ Determine if the given value is "in bounds" for this generator.
        """
        # So our interval is like this: [low_bound, high_bound)
        # The reason is because if we have a generator with a weight of 1, then
        # we only want it to get chosen if value == low bound.  If instead 
        # we used an interval like [low_bound, high_bound], then this generator
        # would get chosen if value==low_bound *OR* value==high_bound; in other
        # words, it would actually wind up having twice the weight that it 
        # should!
        if self.low_bound <= value < self.high_bound:
            return True
        return False
    
    def get_next(self):
        """ Return a randomly-selected source IP address and source port.

        :returns: A tuple; the first value is an IP address (as a string); the
                  second value is a port.
        :rtype: (str,int)
        """
        ip_str = self.addr_generator.get_next()
        port = self.port_generator.get_next()
        return (ip_str, port)


class RandomSourceGenerator:
    """ Class to generate a randomly-generated (source IP, source port) tuple.
    """

    def __init__(self, config_dict_list):
        """ Constructor

        config_dict_list should be a list of configuration dictionaries; see the
        comments for WeightedSourceIpAndPortGenerator.__init__() to see some 
        examples for what these dicts should look like.
        """
        self.weighted_generators = []
        low_bound = 0
        for config_dict in config_dict_list:
            weighted_generator = WeightedSourceIpAndPortGenerator(
                config_dict, low_bound)
            # Sanity check... should never fail...
            assert weighted_generator.low_bound == low_bound
            assert weighted_generator.high_bound > low_bound

            self.weighted_generators.append(weighted_generator)
            low_bound = weighted_generator.high_bound
        return
    
    def get_next(self):
        """ Randomly select the next (source ip, source port).

        :returns: A tuple; the first value is an IP address (as a string); the
                  second value is a port.
        :rtype: (str,int)
        """
        # Note, the bounds on the weighted generator are like this:
        #     [low_bound, high_bound)
        # So the high bound of the last generator is *not* a valid value.
        # So make sure that when we generate a random value, we use
        # high_bound-1 as an upper limit (as opposed to high_bound).
        high_bound = self.weighted_generators[-1].high_bound - 1
        value = random.randint(0, high_bound)

        # So this is O(N).  I don't expect N to ever get very large (maybe a
        # dozen at most...?), so hopefully this less-than-amazing linear
        # search won't be too bad for performance.  If it ever becomes an
        # issue we can always use a tree or something...
        generator = None
        for weighted_generator in self.weighted_generators:
            if weighted_generator.in_bounds(value):
                generator = weighted_generator
                break

        # Should never actually fail....
        assert generator, "Error: Somehow failed to find a generator for " \
            "random value: %s" % value

        ip_str, port = generator.get_next()
        return ip_str, port
