import enum

class Protocol(enum.Enum):
    """ Enumeration of protocol numbers.  A handy list is here:
        https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
    """
    ICMP = 1
    TCP = 6
    UDP = 17
