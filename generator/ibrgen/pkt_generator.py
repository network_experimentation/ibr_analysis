"""

pkt_generator.py

This module contains all the classes for packet generators.  Packet
generators are functions which take packet specifications and send
them out along appropriate channels.  

"""

import logging
import threading
import time
from scapy.all import *

BASE_BACKOFF = 1
class PacketSpec(dict):
    """
    A Packet Specification (PacketSpec) is a gussied up Python dictionary
    that includes two specific fields:
    a default profile (which is a PacketSpec)
    a set of required fields (which is a set) 
    The default profile provides all the fields which are filled in by 
    default in the packet.  The required field specifies all the fields
    which _must_ be filled in the packet.  The packet is treated as a 
    dictionary which is initialized to the default profile and which 
    may then be optionally updated.  In addition, there is a 'correctness'
    value (specified by the required fields) which must be filled out 
    before the packet is good to go.
    """
    def is_correct(self):
        """
        Checks the correctness of a set.  Correctness is defined as
        all values in req_fields are present in active_fields. 
        Returns true if correct; false otherwise.
        """
        # fs is short for 'field set'
        active_fs = set(list(self.keys()))
        req_fs = set(self.req_fields)
        return (active_fs.intersection(req_fs) == req_fs)

    def gen(self):
        pkt = None
        if 'sip' in self and 'dip' in self:
            pkt = IP(dst=self['dip'], src=self['sip'])
            if 'proto' in self:
                if self['proto'] == 17:
                    sport = self['sport']
                    dport = self['dport']
                    pkt = pkt/UDP(sport=sport,dport=dport)
                elif self['proto'] == 6:
                    sport = self['sport']
                    dport = self['dport']
                    pkt = pkt/TCP(sport=sport,dport=dport)
        return pkt

    def send(self):
        send(self.gen(), verbose=False)
        
    def __init__(self, default_profile, req_fields):
        dict.__init__(self, default_profile)
        self.req_fields = req_fields
        
class PacketQueue:
    """
    This is really a simple queue for packet specifications that includes
    thread safety.
    """
    def __len__(self):
        return len(self.queue)
    
    def deq_pkt(self):
        with self._lock:
            pkt = None
            if len(self.queue) > 0:
                pkt = self.queue[0]
                self.queue = self.queue[1:]
            return pkt
        
    def enq_pkt(self,pkt):
        with self._lock:
            if len(self.queue) == self.limit:
                self.queue = self.queue[1:]
            self.queue += [pkt]

    def __init__(self, limit = -1):
        self.queue = []
        self.limit = limit
        self._lock = threading.Lock()

class PacketGenerator:
    """
    The default packet generator just creates packets and 
    dumps them to screen
    """
    def gen_pkt(self, pspec):
        """
        This is the packet generation function that will be overridden
        by the subclasses.  The default version logs the packet to info.
        """
        print("outputting packet ", pspec)
        
    def run(self):
        """
        The run function is the endless loop which is used by the 
        packet generator thread to, ahem, generate packets. 
        """
        while self.count != self.max:
            next_pspec = self.pkt_queue.deq_pkt()
            if next_pspec != None:
                self.gen_pkt(next_pspec)
                self.count += 1
            else:
                time.wait(self.delay)
            
    def __init__(self, delay, max):
        self.pkt_queue = PacketQueue()
        self.delay = delay
        self.max = max
        self.count = 0 
