import yaml, os
from ibrgen.models import *
from ibrgen.ip_addr import *

class Config:
    def parse_model(self, model_data):
        """
        Parses model data.  In the V1 iteration, modelsa are always uniform
        and consist of two values: a pdist (port distribution) and adist
        (address distribution)
        """
        if not 'pdist' in model_data:
            raise ValueError("No value 'pdist' in model spec")
        if not 'adist' in model_data:
            raise ValueError("No value 'adist' in model spec")
        # We know we have both a pdist and adist, so we're
        for descriptor in model_data['pdist']:
            try:
                pspec, prob = descriptor.split(',')[0:2]
                port_number, proto = list(map(int, pspec.split('/')[0:2]))
                prob = float(prob)
                self.model.add_port_dist(port_number, proto, prob)
            except:
                raise Exception("Error parsing port spec '%s', check and reconfigure" %
                                descriptor)
        aphh_min = model_data['adist']['min']
        aphh_max = model_data['adist']['max']
        self.model.set_aphh(aphh_min, aphh_max)
        return
    
    def parse_dspace(self, space_data):
        space_spec = []
        for value in space_data:
            space_spec.append(ibrgen.ip_addr.Subnet(value))
        self.model.space_init(space_spec)
        return
    
    def parse_data(self, yml_data):
        if 'model' in yml_data:
            self.parse_model(yml_data['model'])
        if 'dspace' in yml_data:
            self.parse_dspace(yml_data['dspace'])

    def __init__(self, config_fn):
        self.model = ibrgen.models.NaiveModel()
        if os.path.exists(config_fn):
            with open(config_fn, 'r') as config_f:
                # I'm relying on the YAML exception hierarchy here
                # rather than rolling my own raises
                yml_data = yaml.load(config_f, Loader=yaml.FullLoader)
                self.parse_data(yml_data)
        else:
            raise Exception("File %s not found" % config_fn)

        
