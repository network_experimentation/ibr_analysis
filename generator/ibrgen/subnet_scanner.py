import enum
import heapq
import logging
import multiprocessing as mp
import os
import random
import scapy
import time
import yaml

from scapy.all import IP, TCP, UDP, send

import ibrgen.ip_addr as ip
import ibrgen.protocol_enum as pe
import ibrgen.random_port_generator as rpg
import ibrgen.random_source_generator as rsg


##### Constants

SLEEP_TIME_SECS = 0.001

# Don't let the MIN_SCAN_INTERVAL_SECS be less than SLEEP_TIME_SECS or we
# could get stuck in an infinite loop!
MIN_SCAN_INTERVAL_SECS = SLEEP_TIME_SECS

MAX_IPV4_ADDR = 2**32 - 1


##### Class Definitions

class ScanMode(enum.Enum):
    DEBUG = 1
    SEND_REAL_PACKETS = 2

VALID_SCAN_MODE_STRS = [ x.name for x in ScanMode ]


class ScanInterval:

    def __init__(self, low_bound_secs_, high_bound_secs_):
        """ Constructor

        :param float low_bound_secs_: Represents the minimum frequency (in
                                      seconds) which we will scan an individual
                                      node in the subnet.

        :param float high_bound_secs_: Represents the longest duration (in
                                       seconds) between scans for an individual
                                       node in the subnet.
        """
        low_bound_secs = float(low_bound_secs_)
        high_bound_secs = float(high_bound_secs_)

        # Sanity checks...
        if low_bound_secs < MIN_SCAN_INTERVAL_SECS:
            raise ValueError("Error: Addresses cannot be scanned more "
                "frequently than %s seconds!" % MIN_SCAN_INTERVAL_SECS)
        if high_bound_secs < low_bound_secs:
            raise ValueError("Error: high_bound_secs must be greater than "
                "low_bound_secs!  low_bound_secs: %s.  high_bound_secs: %s"
                % (low_bound_secs_, high_bound_secs_))

        self.low_bound_secs = low_bound_secs
        self.high_bound_secs = high_bound_secs
        return

    def get_next(self):
        """ Get a random period of time which falls within this interval.

        :returns:   A random value between self.low_bound_secs and
                    self.high_bound_secs
        :rtype: float
        """
        retval = random.uniform(self.low_bound_secs, self.high_bound_secs)
        return retval

    def __str__(self):
        return "(%s, %s)" % (self.low_bound_secs, self.high_bound_secs)


class ScanTask:
    def __init__(self, run_at, dest_ip_addr, source_generator, 
                 dest_port_generator, scan_mode=ScanMode.SEND_REAL_PACKETS):
        """ Constructor 

        :param float run_at: Time at which this task should run, as seconds
                             since UNIX epoch.

        :param ip.IPAddr dest_ip_addr: The IP address which should be scanned.

        :param RandomSourceGenerator source_generator: A RandomSourceGenerator
                                                       we should use to generate
                                                       the source IP addr and
                                                       source port for the
                                                       packet that this task
                                                       sends.
        
        :param RandomPortGenerator dest_port_generator: A RandomPortGenerator
                                                        we should use to generate
                                                        the destination port and
                                                        protocol for the packets
                                                        that this task will
                                                        generate.

        :param ScanMode scan_mode: Either ScanMode.DEBUG or 
                                   ScanMode.SEND_REAL_PACKETS.
        """
        dest_port_spec = dest_port_generator.get_next()
        self.dest_port = dest_port_spec.port_num
        self.protocol = dest_port_spec.protocol
        self.src_ip_str, self.src_port = source_generator.get_next()
        self.run_at = run_at
        self.dest_ip_addr = dest_ip_addr
        self.scan_mode = scan_mode

        # Sanity check...
        if self.protocol not in pe.Protocol:
            raise ValueError("Error: protocol must be a value from the "
                "ibrgen.protocol_enum.Protocol enumeration!")
        if self.scan_mode not in ScanMode:
            raise ValueError("Invalid scan mode: Expected one of: %s " 
                % str(VALID_SCAN_MODE_STRS))
        if not isinstance(dest_ip_addr, ip.IPAddr):
            raise ValueError("Error: ip_addr must be an instance of "
                "ibrgen.ip_addr.IPAddr.")
        if not self.src_ip_str:
            raise ValueError("Error: Failed to generate a source IP address "
                "for this task!")
        if self.src_port is None:
            raise ValueError("Error: Failed to generate a source port for "
                "this task!")
        return

    def should_run(self):
        """ Return True if it's time for this task to run.

        :returns: True if it's time for this task to run; False otherwise.
        :rtype: bool
        """
        curr_time = time.time()
        delta_secs = self.run_at - curr_time
        if delta_secs < SLEEP_TIME_SECS:
            return True
        return False

    def __generate_packet(self):
        """ Core function to generate a packet.

        :returns: AN instance of scapy.IP packet.
        :rtype: scapy.IP
        """
        packet = IP(src=self.src_ip_str, dst=str(self.dest_ip_addr))
        if self.protocol == pe.Protocol.TCP:
            packet = packet/TCP(sport=self.src_port, dport=self.dest_port)
        elif self.protocol == pe.Protocol.UDP:
            packet = packet/UDP(sport=self.src_port, dport=self.dest_port)
        else:
            raise ValueError("Error: Don't know how to create packet for "
                "protocol '%s'." % self.protocol.name)

        return packet

    def run(self):
        """ Run this task.

        If self.scan_mode is DEBUG, we will simply log a DEBUG message 
        indicating the packet we would have sent; if self.scan_mode is
        SEND_REAL_PACKETS, then we'll actually send the packet using scapy.
        """
        if self.scan_mode == ScanMode.SEND_REAL_PACKETS:
            packet = self.__generate_packet()
            send(packet, verbose=False)
        elif self.scan_mode == ScanMode.DEBUG:
            logging.debug("ScanTask running in DEBUG mode; task to generate "
                "scan traffic for %s:%s (%s) was just triggered." % (
                self.dest_ip_addr, self.dest_port, self.protocol.name))
        else:
            raise ValueError("Error: Don't know how to run in '%s' mode!"
                % self.scan_mode.name)
        return


class SubnetScanner:
    """ Sends fake "scanning" traffic to an IP subnet at user-specified rates.
    """

    def __init__(self, subnet, scan_interval, source_generator, 
            dest_port_generator, scan_mode=ScanMode.SEND_REAL_PACKETS):
        """ Constructor

        :param Subnet subnet: An instance of ibrgen.ip_addr.Subnet.  This is the
                              subnet that will be scanned.

        :param ScanInterval scan_interval: The ScanInterval object which describes
                                           how often nodes in this subnet will
                                           be scanned.

        :param RandomSourceGenerator source_generator: The RandomSourceGenerator
                                                       that describes how we'll 
                                                       choose randomly-generated
                                                       source IP address and source
                                                       ports for this scanner.

        :param RandomPortGenerator dest_port_generator: The RandomPortGenerator that
                                                        describes which ports we'll
                                                        be scanning for in this
                                                        subnet.

        :param ScanMode scan_mode: Set this to SEND_REAL_PACKETS if you want to
                                   send real packets; set it to DEBUG if you
                                   want to write DEBUG statements to the log
                                   instead of actually sending the packet.
        """
        self.subnet = subnet
        self.scan_interval = scan_interval
        self.source_generator = source_generator
        self.dest_port_generator = dest_port_generator
        self.scan_mode = scan_mode

        # The top of the heap is the next task that needs to run.
        self.heap = []
        return

    def scan_until(self, stop_flag):
        """ Keep scanning until stop_flag.value is set.

        :param mp.Value stop_flag: A multiprocessing.Value instance.  When
                                   stop_flag.value is nonzero, it means that we
                                   should stop scanning.
        """
        self.__populate_heap()
        while stop_flag.value == 0:
            self.__scan(stop_flag)
            time.sleep(SLEEP_TIME_SECS)
        return

    def __queue_task_for_ip(self, dest_ip_addr):
        run_at = time.time() + self.scan_interval.get_next()
        
        task = ScanTask(run_at, dest_ip_addr, self.source_generator, 
            self.dest_port_generator, scan_mode=self.scan_mode)
        self.__push_task(task)
        return

    def __push_task(self, scan_task):
        # The heapq documentation recommends that you push a 3-tuple onto
        # the heap to prevent trouble down the line.  The values of this
        # tuple are:
        #     - The priority of the object (smaller is better)
        #     - A tiebreaker value, in case two entries happen to have the
        #       same priority
        #     - The value you're actually trying to add to the heap.
        # For our purposes:
        #     - The priority is the run_at time.  We want earlier times at
        #       the top of the heap.
        #     - The tiebreaker is just the IP address, as an int, which we
        #       know will be unique.
        #     - The thing we're adding to the heap is an instance of
        #       ScanTask.
        heap_tuple = (scan_task.run_at, scan_task.dest_ip_addr.addr, scan_task)
        heapq.heappush(self.heap, heap_tuple)
        return

    def __pop_task(self):
        # The elements on the heap are:
        #     - run_at: Time when the task should run.
        #     - IP Address: IP address to be scanned, as an int
        #     - ScanTask: ScanTask object which describes the scan we want to
        #       perform
        _, _, scan_task = heapq.heappop(self.heap)
        return scan_task

    def __populate_heap(self):
        # Reset the heap, just in case...
        self.heap = []

        # Add an entry for each IP address in this subnet to the heap.
        for ip_addr in self.subnet:
            self.__queue_task_for_ip(ip_addr)

        return

    def __scan(self, stop_flag):
        scan_task = self.__pop_task()
        while (not stop_flag.value) and scan_task.should_run():
            try:
                scan_task.run()
            except Exception as e:
                logging.exception("Exception detected while attempting to "
                    "send packet to dest IP %s, dest port %s.  Exception: %s"
                    % (str(scan_task.dest_ip_addr), scan_task.dest_port, str(e)))
            # Queue the task to scan this IP again...
            self.__queue_task_for_ip(scan_task.dest_ip_addr)
            scan_task = self.__pop_task()

        # scan_task is now the first task that it's not time to run yet, so
        # put it back in the heap...
        if scan_task:
            self.__push_task(scan_task)

        return


##### Multiprocessing Function

def run_scanner(subnet_str, config_file_path, profile_file_path, stop_flag):
    """ This is the main function that's called as a sub-process by the 
        ScanManager.
    
    See run_scanner_core() for a description of args.  This function is 
    basically just a wrapper to cleanly handle exceptions, Ctl+C, etc...
    """
    try:
        run_scanner_core(subnet_str, config_file_path, profile_file_path,
            stop_flag)
    except KeyboardInterrupt:
        _, filename = os.path.split(profile_file_path)
        logging.info("Sub-process for subnet %s with profile %s got Keyboard "
            "interrupt.  Exiting." % (subnet_str, filename))
    except Exception as e:
        _, filename = os.path.split(profile_file_path)
        logging.exception("Sub-process for subnet %s with profile %s is exiting "
            "due to uncaught exception: %s" % (subnet_str, filename, str(e)))
    finally:
        _, filename = os.path.split(profile_file_path)
        logging.info("run_scanner() is exiting for subnet %s with profile %s."
            % (subnet_str, filename))
    return

def run_scanner_core(subnet_str, config_file_path, profile_file_path, stop_flag):
    """ Main function to start a scanner for the given subnet and profile.
    
    :param str subnet_str: The subnet whose constituent IP addresses should be
                           "scanned".
    
    :param str config_file_path: Path to the configuration file.  See 
                                 etc/config2.yml for an example.
    
    :param str profile_file_path: Path to the "profile" this process should use
                                  when generating traffic; see 
                                  etc/profiles/random_scans.profile.yml for an
                                  example.
    
    :param mp.Value stop_flag: Multiprocessing int value; the parent process
                               should set this to a non-zero value when it
                               wants us to stop sending traffic.
    """
    logging.info(
        "run_scanner() has started with the following params:\n"
        "    subnet_str       : %s\n"
        "    config_file_path : %s\n"
        "    profile_file_path: %s\n"
        % (subnet_str, config_file_path, profile_file_path))

    # Load the config file and the profile.  The config file has global 
    # settings, like DEBUG vs. SEND_REAL_PACKETS mode; the profile has more
    # specific settings, like which ports to scan, what our source IP address
    # ranges should be, etc.
    with open(config_file_path, 'r') as fp:
        config_yaml_data = yaml.load(fp, Loader=yaml.FullLoader)
    with open(profile_file_path, 'r') as fp:
        profile_yaml_data = yaml.load(fp, Loader=yaml.FullLoader)
    
    # Load the RandomSourceGenerator for the source IP addrs + ports for
    # the packets we generate.
    source_generator = rsg.RandomSourceGenerator(
        profile_yaml_data['source_configs'])

    # Load the RandomPortGenerator for the destination ports to scan.
    dest_port_config = profile_yaml_data['dest_port_configs']
    dest_port_generator = rpg.RandomPortGenerator(dest_port_config)

    # Get the scan interval...
    min_secs = profile_yaml_data['scan_interval']['min_secs']
    max_secs = profile_yaml_data['scan_interval']['max_secs']
    scan_interval = ScanInterval(min_secs, max_secs)

    # Read the global DEBUG vs. SEND_REAL_PACKETS mode...
    mode_str = config_yaml_data['global_settings']['mode']
    scan_mode = ScanMode[mode_str]

    # The scanner only scans 1 specific subnet, so get that from the subnet_str
    # arg which is passed in...
    subnet = ip.Subnet(subnet_str)
    scanner = SubnetScanner(subnet, scan_interval, source_generator, 
        dest_port_generator, scan_mode)
    
    # Start scanning!
    scanner.scan_until(stop_flag)
    return
