import random

import ibrgen.ip_addr as ip


##### Helper Classes


class StaticAddressStrategy:
    """ Strategy that always returns the same address every time.  (So it's not
        actually random...)
    """
    def __init__(self, config_dict):
        """ Constructor

        config_dict should look like this:
        {
            "strategy"    : "static_address",
            "address"     : "85.25.103.50"
        }
        """
        self.static_address = config_dict['address']
        if not ip.IPAddr.validate_str_address(self.static_address):
            raise ValueError("Error: The specified static IP address doesn't "
                "look like an IPv4 address: '%s'." % self.static_address)
        return
    
    def get_next(self):
        """ Get the IP address as a string.

        :returns: An IP address.
        :rtype: str
        """
        return self.static_address


class AddressListStrategy:
    """ Randomly selects an IP address from a list of addresses.
    """
    def __init__(self, config_dict):
        """ Constructor

        config_dict should look like this:
        {
            "strategy"     : "address_list",
            "address_list" : [
                "198.20.69.74",
                "198.20.69.98",
                "198.20.70.114"
            ]
        }
        """
        self.addr_list = list(config_dict['address_list'])

        # Sanity checks...
        if not self.addr_list:
            raise ValueError("Error: The address_list must contain at least "
                "one IP address!")
        for addr_str in self.addr_list:
            if not ip.IPAddr.validate_str_address(addr_str):
                raise ValueError("Error: The specified IP address doesn't "
                    "look like an IPv4 address: '%s'." % addr_str)
        return
    
    def get_next(self):
        """ Get a randomly-chosen IP address from our internal list of 
            addresses and return it as a string.

        :returns: An IP address.
        :rtype: str
        """
        retval = random.choice(self.addr_list)
        return retval


class SubnetRangeStrategy:
    """ Randomly choose an IP address from a subnet.
    """

    def __init__(self, config_dict):
        """ Constructor

        config_dict should look like this:
        {
            "strategy"    : "random_address",
            "subnet_range": "167.248.133.0/24"
        }
        """
        subnet_str = config_dict['subnet_range']
        self.subnet = ip.Subnet(subnet_str)
        return
    
    def get_next(self):
        """ Get a randomly-chosen IP address from this subnet, and return it
            as a string.

        :returns: An IP address.
        :rtype: str
        """
        max_val = len(self.subnet) - 1
        rand_val = random.randint(0, max_val)
        rand_ip = self.subnet[rand_val]
        ip_str = str(rand_ip)
        return ip_str


##### Globals

STRATEGY_TO_INST_MAP = {
    "address_list"   : AddressListStrategy,
    "static_address" : StaticAddressStrategy,
    "random_address" : SubnetRangeStrategy,
}


##### Class Definition

class RandomIPAddrGenerator:
    def __init__(self, config_dict):
        strategy_str = config_dict['strategy']
        strategy_cls = STRATEGY_TO_INST_MAP[strategy_str]
        self.strategy = strategy_cls(config_dict)
        return
    
    def get_next(self):
        """ Get the next randomly-chosen IP address, as a string.

        :returns: An IP address.
        :rtype: str
        """
        retval = self.strategy.get_next()
        return retval
