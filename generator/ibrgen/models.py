import ibrgen.pkt_generator
import ibrgen.ip_addr
import random 
allowed_distro_classes = set(['uniform', 'gaussian'])
            
class IBRModel:
    """
    Base IBR Model Class
    An IBR model is a model for a particular class of internet background
    radiation.  These classes take model parameters and then sequentially
    generate packet specs.  Packet specs go to the various packet 
    generators to actually generate the packets. 
    """
    def get_random_address_with_replacement(self):
        """
        get_random_address_with_replacement returns a randomly
        selected address; with replacement means that the chance of
        drawing any particular address is exactly the same each time.  
        """
        result = self.find_address_by_index(random.randrange(self.total_addresses))
        # Purely stubbed out in case of an exception
        self.addresses_gotten += 1
        return result
    
    def get_random_address_without_replacement(self):
        """
        get_random_address_without_replacement pulls addresses 
        from a randomly generated sequence.  Since this method
        has to transparently keep and manage a sequence in the background
        note that this is not memory efficient.
        """
        if self.address_sequence is None or len(self.address_sequence) == 0:
            # Gateway check; make sure that a sequence exists and is viable,
            # if not, create a fresh one.
            self.address_sequence = random.shuffle(
                list(range(0, self.total_addresses)))
        result = self.find_address_by_index(self.address_sequence[0])
        del(self.address_sequence[0])
        self.addresses_gotten += 1
        return result

    def get_linear_address(self):
        """
        get_linear_address is the linear stateful address
        fetch function.  It just returns the next address
        in the counter.
        """
        result = self.find_address_by_index(
            (self.addresses_gotten % self.total_addresses))
        self.addresses_gotten += 1
        return result
    
    def find_address_by_index(self, address_index):
        """
        find_address_by_index returns an address from the 
        address array; this function basically enables us to
        pretend that the disparate IP spaces are one big array. 

        I'm using find to distinguish from get, which are going to be stateful
        operations.
        """
        if address_index >= self.total_addresses:
            raise Exception("index %d is greater than total addresses (%d)" %
                            (address_index, self.total_adddress))
        offset = address_index % self.total_addresses
        last_breakpoint = 0
        for index, breakpoint in enumerate(self.breakpoints):
            if index == 0: # 0 is a no-op, just set the last value
                last_breakpoint = breakpoint
            if (offset >= last_breakpoint) and (offset < breakpoint):
                return self.target_space[index - 1][offset- last_breakpoint]
            else:
                last_breakpoint = breakpoint
            # We should never reach here, return -1 as a stub value
        raise Exception('Logic error with address %d' %address_index)
    
    def space_init(self, space_spec):
        self.target_space = space_spec
        counts = map(len, self.target_space)
        result = [0]
        # This creates an array with the totals in each
        total = 0 
        for i in counts:
            result.append(result[-1] + (i))
            total +=i
        self.breakpoints = result
        self.total_addresses = total
        self.address_sequence = None

    def random_ip(self):
        return "%d.%d.%d.%d" % (random.randint(0,255),
                                random.randint(0,255),
                                random.randint(0,255),
                                random.randint(0,255))
                                
    def next(self):
        next = ibrgen.PacketSpec({'sip': ibrgen.ip_addr.int_ip_to_str(self.src_ip),
                                  'dip': self.get_random_address_with_replacement()},{})
        return next
    
    def __init__(self, target_space):
        self.target_space = target_space
        self.addresses_gotten = 0  
        self.total_addresses = 0
        self.breakpoints = []
        self.space_init()
   

class NaiveModel(IBRModel):
    """
    Naive Model -- Generates packets in a specified distribution.
    """
    def next(self):
        dport, dproto = self.get_port()
        sport = random.randint(4000, 65535)
        result = ibrgen.pkt_generator.PacketSpec({
            'sip': str(ibrgen.ip_addr.IPAddr(random.randint(0,0xFFFFFFFF))),
            'dip': str(self.get_random_address_with_replacement()),
            'sport': sport,
            'dport': dport,
            'proto': dproto,
        },{})
        return result
    
    def get_port(self):
        """ 
        returns a port specification
        """
        port = random.randint(0,65535)
        # Eyeball distro of tcp and udp
        proto = random.choice([6,6,6,6,6,17,17,17])
        die_roll = random.randint(0,100)
        aggregate_threshold = 0
        for cport, cproto, threshold in self.ports:
            aggregate_threshold += threshold
            if die_roll < aggregate_threshold:               
                port = cport
                proto = cproto
                break
        # We failed, return a random port and protocol
        result = (port, proto)
        return result

    def get_aphh_count(self):
        # Gets the address per host-hour count
        return random.randint(self.aphh_min, self.aphh_max)

    def set_aphh(self, min, max):
        self.aphh_min = min
        self.aphh_max = max

    def timesample(self, edur):
        mmin = int((self.total_addresses * self.aphh_min) * (
            (float(edur)/3600)))
        mmax = int((self.total_addresses * self.aphh_max) * (
            (float(edur)/3600)))
        return random.randint(mmin, mmax)
    
    def add_port_dist(self, port, proto, prob):
        self.ports.append((port, proto, prob))

    def __init__(self):
        self.src_ip = None
        self.target_space = None
        self.ports = []
        self.addresses_gotten = 0
        self.total_addresses = 0
        self.breakpoints = []
        
class ExploitScannerModel(IBRModel):
    """
    Simple linear scanner focused on particular exploits
    """

    def __init__(self, src_ip, target_space, portset):
        self.portset = portset
        IBRModel.__init__(self, src_ip, target_space)

                 
        
