'''
ibr_config.py

ibr_config, the ibr configuration file, manages the configuration of
the ibr system.  The IBR configurator consists of one top-level
object, Config.  Config in turn consists of a number of distinct features
for describing the target space, individual modules &c.
'''

import ipaddress, json, logging

class Configuration:
    def network_configuration(self):
        """ 
        network_configuration(self) 
        Loads up network configuration information such as target
        network size.
        """
        logging.info('Configuring network') 
        with open(self.network_file, 'r') as fp:
            config_json = json.load(fp)
            if 'targets' in config_json:
                for element in config_json['targets']:
                    logging.info('Adding network %s to set of targets' %
                                 element)
                    try:
                        new_element = ipaddress.ip_network(element)
                        self.targets.append(element)
                    except:
                        logging.warning('Error parsing address %s' % element)
            else:
                logging.warning(
                    'No elements specifying, setting target space to '
                    'all of IPv4')
                self.targets.append(ipaddress.ip_network('0.0.0.0/0'))
    


    def __init__(self, network_fn, model_fn):
        logging.info('Starting configuration with network file %s, model file %s' % (network_fn, model_fn))
        self.network_file = network_fn
        self.model_file = model_fn
        self.targets = []
        self.network_configuration()
