#!/usr/bin/env python3


import argparse
import ibrgen
import logging
import os
import time

import ibrgen.scan_manager as sm

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",help="Name of configuration file",default='etc/config2.yml')
    parser.add_argument("--logfile", default="ibrgen.log", help="Name of Log file")
    args = parser.parse_args()
    return args

def bkup_log(file_path):
    """ Backup the current log file (if it exists) so that we can start with a 
        fresh log file.
    
    :param str file_path: Path to the log file, which may (or may not) actually
                          exist.
    """
    # Sanity check...
    if not os.path.exists(file_path):
        return

    # So if the name of the logfile is "ibrgen.log", we'll generate names like 
    # these:
    #    ibrgen.log.1.bkup
    #    ibrgen.log.2.bkup
    #    ...etc...
    dir_path, filename = os.path.split(file_path)
    idx = 1
    suffix = ".%d.bkup" % idx
    bkup_filename = filename + suffix
    bkup_file_path = os.path.join(dir_path, bkup_filename)
    while os.path.exists(bkup_file_path):
        idx += 1
        suffix = ".%d.bkup" % idx
        bkup_filename = filename + suffix
        bkup_file_path = os.path.join(dir_path, bkup_filename)

    # OK, now that we've determined what we want to rename the existing logfile
    # to, go ahead and rename it.
    os.rename(file_path, bkup_file_path)
    return

def init_logger(args):
    # First, bkup the old log, if it's there.  If we leave old messages in the 
    # log from several days ago, it messes up the sanity_checker.py script...
    bkup_log(args.logfile)
    assert not os.path.exists(args.logfile)

    log_fmt ='%(asctime)s %(levelname)s %(message)s'
    logging.basicConfig(filename=args.logfile, level=logging.DEBUG,
                        format=log_fmt, datefmt='%Y/%m/%d %H:%M:%S')
    logging.info('Starting IBR Generation')
    logging.info('Configure: configuration: %s, logfile: %s' %( args.config,
        args.logfile))
    return

def main(args):
    scan_mgr = sm.ScanManager(args.config)
    scan_mgr.start_scans()

    print("Scan(s) started; press CTRL+C to halt...")
    try:
        # This process doesn't actually do anything now, except for
        # listening for the CTRL+C.  All of the packet generating/sending
        # is done in subprocesses.
        while True:
            time.sleep(1000)
    except KeyboardInterrupt:
        print("\n\nCTRL+C detected; halting...")
    finally:
        scan_mgr.stop_scans()

    return

if __name__ == '__main__':
    args = get_args()
    init_logger(args)

    main(args)
