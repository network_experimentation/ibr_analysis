import collections
import datetime
import statistics as stats


def get_zero_padded_ip(ip_str):
    tokens = ip_str.split('.')
    assert len(tokens) == 4, "Not an IPv4 string: %s" % ip_str
    retval = "%03d.%03d.%03d.%03d" % (
        int(tokens[0]), int(tokens[1]), int(tokens[2]), int(tokens[3]))
    return retval

PORT_TO_PROTO_MAP = {
    21    : 'ftp',
    22    : 'ssh',
    23    : 'telnet',
    25    : 'smtp',
    53    : 'dns',
    80    : 'http',
    139   : 'netbios',
    443   : 'https',
    631   : 'ipp_or_ipmi',
    2323  : 'telnet',
    3389  : 'remote_desktop',
    5632  : 'pc_anywhere',
    5672  : 'amqp',
    5900  : 'vnc',
    5901  : 'vnc',
    8000  : 'http',
    8080  : 'http',
    8443  : 'https',
    27017 : 'mongo_db',
}
ALL_PROTOCOLS = set(PORT_TO_PROTO_MAP.values())

class PortStats:
    def __init__(self):
        self.proto_to_count_map = collections.defaultdict(int)
        return

    def update(self, port_num_):
        port_num = int(port_num_)
        proto_str = PORT_TO_PROTO_MAP.get(port_num, 'other')
        total = self.proto_to_count_map[proto_str]
        self.proto_to_count_map[proto_str] = (total + 1)
        return

    @property
    def total(self):
        total = 0
        for value in self.proto_to_count_map.values():
            total += value
        return total

    def __str__(self):
        total = self.total
        if total == 0:
            return "No data"
        retval = ""
        protocol_strs = list(sorted(ALL_PROTOCOLS))
        protocol_strs.append('other')  # Always put 'other' last, not alphabetically...
        for proto_str in protocol_strs:
            count = self.proto_to_count_map[proto_str]
            percent = (count / total) * 100.0
            line = "{: <16}: {: >8} ({: >4.1f}%)\n".format(
                proto_str, count, percent)
            retval += line
        return retval

# Dates are like this:
#     2020/11/09 11:08:15
TIMESTAMP_FORMAT_STR = "%Y/%m/%d %H:%M:%S"
class IpStats:
    def __init__(self, ip_addr):
        self.ip_addr = ip_addr
        self.times = []
        self.deltas = []
        return

    def update(self, date_str, time_str):
        datetime_str = date_str.strip() + " " + time_str.strip()
        timestamp_dt = datetime.datetime.strptime(datetime_str,
            TIMESTAMP_FORMAT_STR)
        if self.times:
            assert timestamp_dt >= self.times[-1], "Error: Timestamps must be " \
                "provided in ascending order."
        self.times.append(timestamp_dt)

        # Most of the stats we care about aren't based on the actual time the
        # packet arrived; instead, we tend to care about the time delta between
        # this packet and the previous (or next) one.  So keep a running track
        # of the deltas for interarrival times, too...
        if len(self.times) > 1:
            most_recent = self.times[-1]
            next_most_recent = self.times[-2]
            delta = most_recent - next_most_recent
            self.deltas.append(delta.total_seconds())
        return

    @property
    def num_scans(self):
        return len(self.times)

    @property
    def mean_scan_interarrival_secs(self):
        if self.deltas:
            retval = stats.mean(self.deltas)
        else:
            retval = -1
        return float(retval)

    @property
    def min_scan_interarrival_secs(self):
        if self.deltas:
            retval = min(self.deltas)
        else:
            retval = -1
        return float(retval)

    @property
    def max_scan_interarrival_secs(self):
        if self.deltas:
            retval = max(self.deltas)
        else:
            retval = -1
        return float(retval)

    def __str__(self):
        retval = "{:15}: total scans: {:6}  " \
            "mean interarrival (secs): {:>6.2f}  " \
            "min interarrival (secs): {:>6.2f}  " \
            "max interarrival (secs): {:>6.2f}".format(
                self.ip_addr, self.num_scans,
                self.mean_scan_interarrival_secs,
                self.min_scan_interarrival_secs,
                self.max_scan_interarrival_secs )
        return retval

class IpStatMgr:
    def __init__(self):
        self.ip_to_stats_map = {}
        return

    def update(self, ip_str, date_str, time_str):
        if ip_str in self.ip_to_stats_map:
            ip_stats = self.ip_to_stats_map[ip_str]
        else:
            ip_stats = IpStats(ip_str)
            self.ip_to_stats_map[ip_str] = ip_stats
        ip_stats.update(date_str, time_str)
        return

    def get_all_ip_stats(self):
        retval = list(self.ip_to_stats_map.values())
        sorted_retval = sorted(retval, key=lambda x: get_zero_padded_ip(x.ip_addr))
        return sorted_retval

with open('ibrgen.log', 'r') as fp:
    port_stats = PortStats()
    ip_stat_mgr = IpStatMgr()
    for line in fp:
        line = line.strip()
        if "ScanTask running in DEBUG mode" not in line:
            continue
        tokens = line.split()
        ip_and_port_str = tokens[14]
        ip_str, port_str = ip_and_port_str.split(':')
        port_num = int(port_str)
        port_stats.update(port_num)

        date_str = tokens[0]
        time_str = tokens[1]
        ip_stat_mgr.update(ip_str, date_str, time_str)

    print(str(port_stats))
    ip_stats_list = ip_stat_mgr.get_all_ip_stats()
    for ip_stats in ip_stats_list:
        print(ip_stats)
