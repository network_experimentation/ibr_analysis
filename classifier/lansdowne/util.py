"""
lansdowne.util

Utility functions for the lansdowne system.  This includes things like
reading configurations, running SiLK commands, &c.
"""
import logging
import os
import tempfile
import argparse
import time
import yaml
import sys

from . import date_glob
colors = {
    'black': u'\u001b[30m',
    'red': u'\u001b[31m',
    'green': u'\u001b[32m',
    'yellow': u'\u001b[33m',
    'blue': u'\u001b[34m',
    'magenta': u'\u001b[35m',
    'cyan': u'\u001b[36m',
    'white': u'\u001b[37m',
    'reset': u'\u001b[0m',
}

epithets = [
    ('normset', 'Setfile of normal addresses'),
    ('allset','Setfile of all addresses'),
    ('shortset', 'Set of addresses which do too little to model alone'),
    ('shorttxt','Short textfile'),
    ('normtxt','Normal textfile'),
    ('shortrwf', 'RWF file of short activity'),
    ('shortph','Short placeholder file, can be rewritten with impunity'),
    ('knownset','Set of addresses which are known legit scanners'),
    ('knownrwf','RWF file of legit scanners'),
    ('bsset','Set of backscatter sources'),
    ('bsrwf','RWF file of backscatter traffic'),
    ('icmpbsset','set file of icmp backscatter'),
    ('intbsrwfa','First interim bs rwf'),
    ('intbsrwfb','Second interim bs rwf'),
    ('tcpscanrwf','tcp scan rwf'),
    ('tcpscanset','Scanner set'),
    ('unclassified','Unclassified'),
    ('workrwf', 'Working rwf'),
    ('tcpset','Set of all tcp ips'),
    ('oopset','Set of all unknown ips'),
    ('udpset','Set of all udp ips'),
    ('udpscanset','Set of all udp scanning ips'),
    ('icmpset','Set of all icmp ips'),
    ('icmpscanset','Set of all icmp scanning ips'),
    ('multiset','set of all multiprotocol ips'),
    ('holdset','Temporary holding set for operations'),
    ('holdtxt','Temporary holding textfile for operations'),
    ('holdrwf','Temporary holding rawfile for operations'),
    ('intersectreport','Intersection Report'),
]
def die(msg):
    sys.stdout.write(colors['yellow'] + msg + colors['reset'] + '\n')
    sys.exit(-1)
    
def init_parser(progname, desc):
    """
    init_parser(progname: string, desc:string) --> argparse.ArgumentParser
    Creates an argument parser which contains a base set of 
    arguments which are common to all lans applications.
    
    """
    default_config_file = os.path.join(os.getcwd(), 'config.yml')
    default_log_file = os.path.join(os.getcwd(), 'lansdowne.log')
    result = argparse.ArgumentParser(prog = progname, description = desc)
    result.add_argument('-s', '--startdate', type = str,
                        default = time.strftime('%Y/%m/%d'),
                        help =
                        'Date to begin analysis in YYYY/MM/DD foramt.  Default is today')
    result.add_argument('-e', '--enddate', type = str,
                        default = time.strftime('%Y/%m/%d'),
                        help =
                        'Date to end analysis in YYYY/MM/DD format, default is today')
    result.add_argument('-c', '--config', type = str, default = default_config_file, 
                        help =
                        ('Configuration file location, defaults is %s'
                         % default_config_file))
    result.add_argument('-l', '--loglevel', type = int, default = logging.INFO,
                        help = 'Default log level, set to logging.INFO')
    result.add_argument('-f', '--logfile', type = str, default = default_log_file,
                        help =
                        ('Log file location, defaults to %s' % default_config_file))
    result.add_argument('--dry-run', help =
                        ('Determines whether or not the run is a dry run'),
                        action='store_true')
    return result

def global_cstate_from_args(arguments):
    """ 
    init_from_args
    Initializes from the command line arguments.
    """
    result = {}
    result['log_fn'] = arguments.logfile
    result['log_lv'] = arguments.loglevel
    result['conf_fn'] = arguments.config
    result['start_date'] = date_glob.parse_date_option(arguments.startdate)
    result['end_date'] = date_glob.parse_date_option(arguments.enddate)
    result['date'] = time.strftime("%Y/%m/%d", time.localtime(time.time()))
    if arguments.dry_run:
        result['is_dry_run'] = True
    else:
        result['is_dry_run'] = False
    return result
    
def init(cstate):
    """
    init
    Sets the low level initialization (from the command line), this covers
    anything that happens before reading the main configuration file and is
    defined by the CL arguments.
    """
    if not os.path.exists(cstate['conf_fn']):
        print('Error with command line, file %s does not exist' % cstate['conf_fn'], file = sys.stderr)
        sys.exit(-1)
    #
    # Initialize Logging
    #
    logging.basicConfig(filename = cstate['log_fn'], encoding = 'utf-8', level = cstate['log_lv'],
                        format = '%(asctime)s %(message)s', datefmt = '%Y/%m/%d:%H:%M:%S')
    logging.info('Starting LANSDOWNE process with pid %d ' % os.getpid())
    logging.info('Initial configuration complete, starting full configuration')
    return cstate

def global_load_config(cstate):
    logging.info('Opening configuration file %s' % cstate['conf_fn'])
    with open(cstate['conf_fn'], 'r') as conf_fh:
        data = yaml.load(conf_fh, Loader = yaml.FullLoader)
        for key in data:
            cstate[key] = data[key]
    logging.debug("Update cstate: %s" % str(cstate))
    # Check for crit-fail-if missing variables: data, target_set
    for i in ['data', 'target_set', 'reports']:
        if not i in cstate:
            logging.critical('Missing critical variable %s' % i)
            sys.exit(-1)
    logging.info('Verifying data directory (%s) ...' % cstate['data'])
    return cstate

def source_data_string(data_root, day_of_query):
    """
    source_data_string(data_root, day_of_query) --> string
    Returns a strang which globs together all the potential files. 
    Does not guarantee that any files exist.
    """
    return os.path.join(data_root, day_of_query, '*rwf*')

def make_report_directory(cstate, file_info, day):
    """
    make_report_directory
    
    Creates a reports directory; a report directory is organized YYYY/MM/DD, where YYYY, MM, DD are
    directories for year, month, date (zero packed)
    """
    tgt = os.path.join(cstate['reports'], day)
    if not os.path.exists(tgt):
        os.makedirs(tgt, exist_ok = True)
    # As part of the move towards multiple target sets, the new directories
    # here form part of the structure
    for i in ['reports', 'sets']:
        subdir = os.path.join(tgt, i)
        if not os.path.exists(subdir):
            os.makedirs(subdir, exist_ok = True)

def fetch_tf_name(directory, prefix):
    """
    fetch_tf_name(directory, prefix)
    Create a temporary filename matching the directory and prefix requirements.
    We're primarily using tf's to pass names to silk, so we don't need the filehandles
    so much as the name (this also means we're explicitly unsafe, c'est la vie).  
    """
    
    tfile = tempfile.NamedTemporaryFile(dir = directory, prefix = prefix)
    tname = tfile.name
    tfile.close()
    return tname

def create_workdirs(cstate):
    """
    create_workdirs
    This function creates working directories and filenames for analysis; these
    are all named temporary files and are created with a predictable scheme
    that uses the date and pid as roots.  
    """
    logging.info('Creating working directories')
    # The working directory structure is as follows
    # ./ -  Working location from config, defaults to './working'
    # ./date_pid/
    # Then individual files under here.  Each file has an 'epithet', which describes waht the file is.
    # epithets are in the name and in the structure created here
    result = {}
    # for the sake of the logging, each of these epithets also has a descriptor. 
    for i, desc in epithets:
        # Note that all these files are persistent
        logging.info('Generating file %s: %s' % (i,desc))
        tfile = tempfile.NamedTemporaryFile(dir = cstate['work_dir'], delete = False, prefix = '%s_%s_' % (cstate['work_prefix'], i))
        tname = tfile.name
        tfile.close()
        logging.info('%s file name: %s' % (i, tname))
        warn_del(tname)
        # I'm using the name only
        result[i] = tname
    return result

def warn_del(fn):
    """
    Utility function; it checks to see if a particular file exists, if it does, issue a warning 
    before unlinking, then unlink.  Idempotency for fractured runs
    """
    if os.path.exists(fn):
        logging.warning('File %s already exists, so unlinking' % fn)
        os.unlink(fn)

def protect_file(file_path):
    """
    protect_file(file_path)
    
    gzips the file, and converts permissions to 444
    """
    zip_fn = file_path + '.gz'
    os.system("gzip %s" % file_path)
    os.chmod(zip_fn, 0o444)
    
def run_silk_command(cmd):
    """
    run_silk_command
    This is a logging shell that I wrap around every silk command, to make
    sure I log start and finish.
    """
    logging.info('Running command [%s]' % cmd)
    os.system(cmd)
    logging.info('Command finished')
    return

def read_setfile(set_fn):
    logging.info('ACTION: reading setfile %s' % set_fn)
    with os.popen('rwsetcat %s' % set_fn, 'r') as datapipe:
        result = set(list(map(lambda x:x[:-1], datapipe.readlines())))
    logging.info('Read complete, found %d ips' % (len(result)))
    return result

def get_set_count(set_fn):
    if os.path.exists(set_fn):
        with os.popen('rwsetcat --count-ip %s' % (set_fn), 'r') as datapipe:
            dstring = datapipe.readline()
            try:
                d = int(dstring)
            except:
                logging.error('Attempt to parse string |%s| as integer, marking as zero')
                return 0
            return d
    else:
        logging.error('No file %s, return zero' % set_fn)
        return 0
    
def get_set_intersect(set_fn_a, set_fn_b):
    if os.path.exists(set_fn_a) and os.path.exists(set_fn_b):
        with os.popen('rwsettool --intersect %s %s | rwsetcat --count-ip' % (set_fn_a, set_fn_b), 'r') as datapipe:
            dstring = datapipe.readline()
            try:
                d = int(dstring)
            except:
                logging.error('Attempt to parse string |%s| as integer, marking as zero')
                return 0
            return d
    else:
        logging.error('No file %s, return zero' % (set_fn_a + '/' + set_fn_b))
        return 0 
