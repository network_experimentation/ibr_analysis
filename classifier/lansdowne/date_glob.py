#!/usr/bin/env python
#
# Date globbing library
import os, sys, datetime, time

class FDIterable:
    def __iter__(self):
        return self

    def __next__(self):
        if self.cd <= self.end_date:
            result = self.cd
            self.cd += self.td
            return day_to_dir(self.fileroot, result.year, result.month, result.day)
        else:
            raise StopIteration
        
    def __init__(self, sd, ed, fr):
        self.fileroot = fr
        self.start_date = sd
        self.end_date = ed
        self.td = datetime.timedelta(days = 1)
        self.cd = self.start_date
        
def day_to_dir(file_root, year, month, date ):
    """
    day_to_dir(string directory, year, month date integers)
    The basic date to directory function, returns a directory
    for that YMD combo
    """
    result = os.path.join(file_root, "%04d" % year, "%02d" % month, "%02d" % date)
    return result

def day_range(start_date, end_date, file_root = ''):
    """
    day_range(file_root [dir], start_date [datetime object], end_date [datetime object])
    returns an iterator which will generate all the values from start_date to end_date
    inclusive 
    so start_date=2020/09/01 end_date=2020/09/01 will return [2020/09/01]
    start_date = 2020/09/01 end_date=2020/09/03 will return [2020/09/01, 2020/09/02, 2020/09/03] 
    Note that file_root is optional.
    """
    return FDIterable(start_date, end_date, file_root)

def parse_date_option(input_date):
    """
    Interpret date options and convert them into a sequence of 
    datetime values
    """
    tm_rep = time.strptime(input_date, "%Y/%m/%d")
    result = datetime.datetime(tm_rep.tm_year, tm_rep.tm_mon, tm_rep.tm_mday)
    return(result)
