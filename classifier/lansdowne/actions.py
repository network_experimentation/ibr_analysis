# actions.py
#
# This module contains all the individual actions available to the
# system

import csv
import os
import glob
import logging
import sys

from . import util

def get_set_count(set_fn):
    if os.path.exists(set_fn):
        with os.popen('rwsetcat --count-ip %s' % (set_fn), 'r') as datapipe:
            dstring = datapipe.readline()
            try:
                d = int(dstring)
            except:
                logging.error("Attempt to parse string |%s| as integer, marking as zero")
                return 0
            return d
    else:
        logging.error("No file %s, return zero" % set_fn)
        return 0

def gen_pop_csv(config, day, dry_run):
    """ 
    Given a report directory, conver the sets into a csv file
    """
    print("In gen_pop_csv")
    repdir = os.path.join(config['reports'], day)
    data_files = glob.glob(os.path.join(repdir, 'sets', '*set.gz'))
    print("data_files are %s" % (str(list(data_files))))
    results = {}
    
    for i in data_files:
        print(i)
        results[os.path.basename(i)] = get_set_count(i)
        
    with open(os.path.join(config['reports'], day, 'reports', 'setcounts.csv'),'w') as setfile:
        c = csv.DictWriter(setfile, fieldnames = list(results.keys()))
        c.writeheader()
        c.writerow(results)
    return


def gen_timeseries(config, day, dry_run):
    """
    Given an existing report directory, generate a, hourly and daily timeseries (.cnt)
    for the corresponding sets
    """
    mainkeepers = ['normset','shortset','bsset','allset','knownset','multiset','tcpscanset','udpscanset','icmpscanset','oopset']
    logging.info('ACTION: gen_timeseries')
    repdir = os.path.join(config['reports'], day)
    data_string = util.source_data_string(config['data'], day)
    for i in mainkeepers:
        logging.info("Looking for %s set in directory %s" % (i, os.path.join(repdir, 'sets')))
        source_setfile = os.path.join(repdir, 'sets', i + '.gz')
        if os.path.exists(source_setfile):
            logging.info("Setfile exists")
        else:
            logging.error("NO such setfile")
        tgt_report = os.path.join(repdir, 'reports', i + '.cnt')
        if os.path.exists(os.path.join(source_setfile)):
            cmd = "rwfilter --sip=%s %s --pass=stdout | rwcount --bin-size=3600 > %s" % (
                source_setfile, data_string, tgt_report)
            util.run_silk_command(cmd)
    logging.info('Exiting')

def gen_ddos_report(config, limit, day, dry_run):
    """
    gen_ddos_report_scan(config, n, day)
    generates a ddos report in csv format, the report
    """
    # Step 1, query on tcp scanners
    rep_dir = os.path.join(config['reports'], day)
    data_string = util.source_data_string(config['data'], day)
    results = []
    logging.info("Beginning DDoS Report generation for day %s" % day)
    if dry_run:
        logging.info("Dry run for this DDoS Report, aborting")
    else:
        logging.info("Querying report")
        cmd = (("rwfilter --sip=%s --proto=1,6 --pass=stdout  %s| rwuniq --field=sip " +
                " --values=packets,dip-distinct,stime-earliest,etime-latest --no-ti") %
               (os.path.join(rep_dir, 'sets', 'bsset.gz'), data_string))
        silk_pipe = os.popen(cmd, 'r')
        for line in silk_pipe.readlines():
            bs_ip, pkts, dips, earliest_time, latest_time = list(line.split('|')[0:5])
            # Should only get one entry, this IS rwuniq output
            results.append([bs_ip, int(pkts), int(dips), earliest_time, latest_time])
        logging.info("Read %d lines" % len(results))
        s = sorted(results, key = lambda x:-x[2])
        output = os.path.join(rep_dir, 'reports', 'ddos.csv')
        logging.info("Writing the top %d" % limit)
        with open(output, 'w') as tgt_file:
            field_names = ['ip','packets','dips','earliest','latest']
            tgt_write = csv.DictWriter(tgt_file, fieldnames = field_names)
            tgt_write.writeheader()
            for x in s[0:limit]:
                tgt_line = {'ip':x[0],'packets':x[1],'dips':x[2],'earliest':x[3],'latest':x[4]}
                tgt_write.writerow(tgt_line)
    logging.info("Finished ddos report")

def gen_topn_udp_scan(config, limit, day, dry_run):
    """
    gen_topn_tcp_scan(config, n, day)
    Generates a tcp scan report in csv format, the report consists of: 
    Destination port, # of source Ips, # of packets, 16ths
    """
    # Step 1, query on tcp scanners
    logging.info("Beginning UDP TOP-N scan Report generation for day %s" % day)
    repdir = os.path.join(config['reports'], day)
    data_string = util.source_data_string(config['data'], day)
    results = []
    if dry_run:
        logging.info("Dry run for this UDP TOP-N scan report, aborting")
    else:
        cmd = (("rwfilter --sip=%s --proto=17 --pass=stdout  %s| rwuniq --field=dport " +
                " --values=sip-distinct,packets --no-ti") %
               (os.path.join(repdir, 'sets', 'udpscanset.gz'), data_string))
        silk_pipe = os.popen(cmd, 'r')
        for line in silk_pipe.readlines():
            scan_port, srcs, pkts = list(map(int, line.split('|')[0:3]))
            results.append([scan_port, pkts, srcs]) # Should only get one entry, this IS rwuniq output
        s = sorted(results, key = lambda x:-1*x[1])
        output = os.path.join(repdir, 'reports', 'udpscan.tgt_port.csv')
        with open(output, 'w') as tgt_file:
            field_names = ['port','sources','packets']
            tgt_write = csv.DictWriter(tgt_file, fieldnames = field_names)
            tgt_write.writeheader()
            for i in s[0:limit]:
                tgt_line = {'port': i[0], 'sources': i[2], 'packets': i[1]}
                tgt_write.writerow(tgt_line)
    logging.info("Finished UDP TOP-N Scan report") 

def gen_topn_tcp_scan(config, limit, day, dry_run):
    """
    gen_topn_tcp_scan(config, n, day)
    Generates a tcp scan report in csv format, the report consists of: 
    Destination port, # of source Ips, # of packets, 16ths
    """
    # Step 1, query on tcp scanners
    logging.info("Beginning TCP TOP-N scan Report generation for day %s" % day)
    repdir = os.path.join(config['reports'], day)
    data_string = util.source_data_string(config['data'], day)
    results = []
    if dry_run:
        logging.info("Dry run for this TCP TOP-N scan report, aborting")
    else:
        cmd = (("rwfilter --sip=%s --proto=6 --pass=stdout --flags-all=S/SAFR %s| rwuniq " +
                " --field=dport --values=sip-distinct,packets --no-ti") %
               (os.path.join(repdir, 'sets', 'tcpscanset.gz'), data_string))
        silk_pipe = os.popen(cmd, 'r')
        for line in silk_pipe.readlines():
            scan_port, srcs, pkts = list(map(int, line.split('|')[0:3]))
            results.append([scan_port,pkts, srcs]) # Should only get one entry, this IS rwuniq output
        s = sorted(results, key = lambda x:-1*x[1])
        output = os.path.join(repdir, 'reports', 'tcpscan.tgt_port.csv')
        with open(output, 'w') as tgt_file:
            field_names = ['port','sources','packets']
            tgt_write = csv.DictWriter(tgt_file, fieldnames = field_names)
            tgt_write.writeheader()
            for i in s[0:limit]:
                tgt_line = {'port': i[0], 'sources': i[2], 'packets': i[1]}
                tgt_write.writerow(tgt_line)
    logging.info("Finished TCP top-N scan report") 

def get_status(config, limit, day, dry_run):
    """
    get_status: provides a status breakdown on an individual directory. 
    """
    # These are the statuses I check
    raw_present = False
    partitioned = 0
    report_dir_present = False
    set_dir_present = False
    # Status: check to see whether raw files are there
    data = util.source_data_string(config['data'], day)
    if len(glob.glob(data)) > 0:
        raw_present = True
    # While rp and part should be implicit (i.e., if !rp, then
    # !part), it's possible someone will lose raw data while a partition
    # exists, so I'm treating them as orthogonal for now.
    repdir = os.path.join(config['reports'], day, 'reports')
    setdir = os.path.join(config['reports'], day, 'sets')
    if os.path.exists(setdir):
        set_dir_present = True
        setfiles = os.path.join(setdir, '*gz')
        if len(glob.glob(setfiles)) > 0:
            partitioned = len(glob.glob(setfiles))
    if os.path.exists(repdir):
        report_dir_present = True
    
    sys.stderr.write("Date: %16s Raw: %s Reports: %s Sets: %s Partitioned: %d\n" % (day, str(raw_present), str(report_dir_present), str(set_dir_present), partitioned))
    return (raw_present, report_dir_present, partitioned)
        
