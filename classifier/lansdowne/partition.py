"""
partition.py

This is the main classifier function; it takes the incoming traffic
and reaks it into a set of subclasses based on the traffic's behavior,
the results can then be read by reports.
"""

import os
import logging
from . import util 

def gen_partition(config, day, dry_run):
    logging.info("Beginning partition process for %s" % day)
    if 'target_sets' in config:
        for sname, spath in config['target_sets']:
            logging.info("Processing target set %s at %s") 
    file_info = util.create_workdirs(config)
    isolate_aberrants(config, file_info, day, dry_run)
    generate_first_pass_data(config, file_info, day, dry_run)
    partition(config, file_info, day, dry_run)
    cleanup(config, file_info, day)

def isolate_aberrants(config, file_info, day, dry_run):
    """
    isolate_aberrants()
    First pass isolation, pops out the following sets:
    * Multiprotocol hosts  --- multiset
    * Hosts using unusual protocols (not 1,6,17) -- oopset
    * UDP, ICMP, TCP hosts -- udpset, icmpset, tcpset
    """
    logging.info('ACTION: Beginning High Level Isolation of Aberrants')
    if dry_run:
        logging.info('DRY RUN, not doing anything')
    else:
        data_string = util.source_data_string(config['data'], day)
        cmd = 'rwfilter --proto=1,6,17 --fail=stdout --dipset=%s %s | rwset --sip-f=%s' % (
            config['target_set'], data_string, file_info['oopset'])
        logging.info('Pulling out out of protocol actions') 
        util.run_silk_command(cmd)
        logging.info('Generating TCP, UDP and ICMP files')
        for pnum,pname in [(1,'icmpset'),(6,'tcpset'),(17,'udpset')]:
            cmd = 'rwfilter --proto=%d --pass=stdout %s | rwset --sip-f=%s' % (
                pnum, data_string, file_info[pname])
            util.run_silk_command(cmd)
        fulltable = {}
        logging.info('Now counting multiprotocol addresses')
        for i in ['icmpset','oopset','tcpset','udpset']:
            data = util.read_setfile(file_info[i])
            for j in data:
                if not j in fulltable:
                    fulltable[j] = 1
                else:
                    fulltable[j] += 1
        res_elt = dict(filter(lambda x:x[1] >= 2, fulltable.items()))
        util.warn_del(file_info['holdtxt'])
        if len(res_elt) > 0:
            logging.info('%d ips appear to be multiprotocol' % len(res_elt))
            with open(file_info['holdtxt'],'w') as temptxtfile:
                for i in res_elt.keys():
                    temptxtfile.write('%s\n' % i)
            cmd = 'rwsetbuild %s %s' % (file_info['holdtxt'], file_info['multiset'])
            util.run_silk_command(cmd)
            util.warn_del(file_info['holdtxt'])
    logging.info('Exiting Aberrant Isolation')
    return

def workrwf_qs(config, day, dry_run):
    """
    workrwf_qs(config, file_set, day, dry_run)
    Generates trhe working rawfile query set, which is the 
    files in the working directory without the known set if
    such a beast exists.
    
    This should reduce the disk quota abuse I've been doing in favor
    of direct queries of the converted data. 
    """
    day_string = util.source_data_string(config['data'], day)
    if 'known_set' in config:
        if os.path.exists(config['known_set']):
            q_string = "--not-sipset=%s %s" % (config['known_set'], day_string)
        else:
            q_string = day_string
    return "rwfilter %s --pass=stdout " % q_string

def generate_first_pass_data(config, file_set, day, dry_run):
    """
    generate_first_pass_data()
    The first pass data splits out short and known activity. This is the first
    time we do actual partitioning.
    """
    logging.info('ACTION: generate_first_pass_data')
    if dry_run: 
        logging.info('DRY RUN, not doing anything')
    else:
        data_string = util.source_data_string(config['data'], day)
        # Step one, identify and isolate the known traffic
        kstring = ''
        if not 'known_set' in config:
            logging.warning('No known IP set included, skipping known IP analysis')
        else:
            kstring = '--sipset=%s' % config['known_set']

        # This section ge
        cmd = 'rwfilter --dipset=%s --pass=%s %s %s --all=stdout | rwset --sip-f=%s' % (
            config['target_set'], file_set['knownrwf'], kstring, data_string, file_set['allset']) 

        util.warn_del(file_set['knownrwf'])
        util.warn_del(file_set['allset'])
        util.run_silk_command(cmd)

            # Create the knwon set        
        cmd = 'rwfilter --pass=stdout --proto=0-255 %s | rwset --sip-file=%s' % (
            file_set['knownrwf'], file_set['knownset'])
        util.warn_del(file_set['knownset'])
        util.run_silk_command(cmd)
        util.warn_del(file_set['shortph'])
        cmd = ('rwfilter --not-sip=%s --pass=stdout %s | rwuniq --field=1 ' 
               ' --values=packets,dip-distinct --no-title > %s') % (
                   config['known_set'], data_string, file_set['shortph'])
        util.run_silk_command(cmd)
        # We should now have an output file 
        if os.path.exists(file_set['shortph']):
            analyze_short_placeholder_data(config, file_set)
        else:
            logging.error('No short data exists, aborting')
            sys.exit(-1)
    logging.info('Finishing generate_first_pass_data')
    return

def partition(config, file_info, day, dry_run):
    """
    partition
    We apply the breakdowns from first-class data to produce partitions that make senss for further analysis
    """
    logging.info('ACTION: PARTITION')
    if dry_run:
        logging.info('DRY RUN, not doing anything')
    else:
        # at this point, we're going to break out tcp scans, tcp backscatter, and udp scans
        # Let's build our TCP scans
        # First we cut out tcp scans
        util.warn_del(file_info['tcpscanset'])
        util.warn_del(file_info['tcpscanrwf'])
        util.warn_del(file_info['intbsrwfa'])
        util.warn_del(file_info['intbsrwfb'])
        util.warn_del(file_info['unclassified'])
        util.warn_del(file_info['bsset'])
        # Isolate TCP, UDP and ICMP scanners, note that workrwf has known pulled out already 
        cmd = '%s | rwfilter --input-pipe=stdin --not-sipset=%s --proto=6 --flags-all=S/SA --pass=stdout | rwset stdin --sip=%s' % (
            workrwf_qs(config, day, dry_run),
            file_info['shortset'],
            file_info['tcpscanset'])
        util.run_silk_command(cmd)
        # We assume that all UDP activity is scanning
        cmd = '%s | rwfilter --input-pipe=stdin --not-sipset=%s --proto=17 --pass=stdout | rwset stdin --sip=%s' % (
            workrwf_qs(config, day, dry_run), file_info['shortset'], file_info['udpscanset'])
        util.run_silk_command(cmd)
        # For ICMP activity, we have to split out ping and TTL exceeded
        cmd = '%s | rwfilter --input-pipe=stdin --sipset=%s --pass=stdout --proto=1 | rwfilter --input-pipe=stdin --dport=2048 --pass=stdout | rwset stdin --sip=%s' % (
            workrwf_qs(config, day, dry_run),file_info['icmpset'],  file_info['icmpscanset'])

        util.run_silk_command(cmd)
        cmd = '%s | rwfilter --input-pipe=stdin --sipset=%s --pass=stdout --proto=1 | rwfilter --input-pipe=stdin --dport=0,778 --pass=stdout | rwset stdin --sip=%s' % (
            workrwf_qs(config, day, dry_run), file_info['icmpset'],  file_info['icmpbsset'])
        util.run_silk_command(cmd)    
        cmd = '%s | rwfilter --input-pipe=stdin --not-sipset=%s --pass=stdout | rwfilter --input-pipe=stdin --proto=6 --not-sipset=%s --pass=stdout | rwfilter --input-pipe=stdin --flags-all=A/A --pass=%s --fail=stdout | rwfilter --input-pipe=stdin --flags-all=R/R --pass=%s --fail=%s' % (
            workrwf_qs(config, day, dry_run), file_info['shortset'], file_info['tcpscanset'], file_info['intbsrwfa'], file_info['intbsrwfb'], file_info['unclassified'])
        util.run_silk_command(cmd)
        cmd = 'rwcat %s %s > %s ' % (file_info['intbsrwfa'], file_info['intbsrwfb'], file_info['bsrwf'])
        util.run_silk_command(cmd)
        cmd = 'rwfilter --proto=0-255 --pass=stdout %s | rwset --sip-f=%s' % (file_info['bsrwf'], file_info['bsset'])
        util.run_silk_command(cmd)
    logging.info('Exiting gen_partition')
    return

def analyze_short_placeholder_data(config, file_set):
    """
    analyze_short_placeholder_data
    Rips apart the IP addresses in the short placeholder and breaks out the
    short and pathological sets
    """
    # set short and pathological thresholds; use config values, default to
    # 5 (for short) and 4608 (6 * 768) for pathological
    # Note; current path threshold is tied to ISI darkspace size
    logging.info('Starting analyze short placeholder')
    sh_c = 0 # short count
    pt_c = 0 # pathological count
    nm_c = 0  # normal count
    sh_th = 5 if not 'short_threshold' in config else config['short_threshold'] 
    logging.info('short thresh %d' % (sh_th))
    util.warn_del(file_set['shorttxt'])
    util.warn_del(file_set['normtxt'])
    util.warn_del(file_set['shortset'])
    util.warn_del(file_set['normset'])
    fh_s = open(file_set['shorttxt'],'w')
    fh_n = open(file_set['normtxt'],'w')
    a = open(file_set['shortph'])
    for i in a.readlines():
        source, p_count, dip_count = i[:-1].split('|')[0:3]
        p_count = int(p_count)
        dip_count = int(dip_count)
        if p_count < sh_th:
            fh_s.write("%s\n" % source)
            sh_c += 1
        else:
            fh_n.write("%s\n" % source)
            nm_c += 1
    fh_s.close()
    fh_n.close()
    util.run_silk_command('rwsetbuild %s > %s' % (file_set['shorttxt'],file_set['shortset']))
    util.run_silk_command('rwsetbuild %s > %s' % (file_set['normtxt'],file_set['normset']))
    logging.info('Partitioned data into %d short %d normal' % (
        sh_c, nm_c))

def cleanup(config, file_info, day):
    """
    cleanup
    - config: hashtable, configuration paratmers
    - file_info: hashtable, list of all files
    - day: string, day we're processing in YYYY/MM/DD format

    Cleanup goes through the working directory, cleans out all the files, 
    preserves any files its ordered to preserve by config (eventually also 
    a command line parm).

    The function is written to be idempotent, so it shouldn't care about dry run parameters.
    """
    logging.info('Beginning cleanup')
    repdir = os.path.join(config['reports'], day)
    util.make_report_directory(config, file_info, day)
    # First thing we do is steal the keepers
    if config['keepers'] != []:
        logging.info('There exist keepers we will copy: %s' % (','.join(config['keepers'])))
        for i in config['keepers']:
            if os.path.exists(file_info[i]): 
                file_dest = os.path.join(repdir, 'sets', i)
                os.rename(file_info[i], file_dest)
                util.protect_file(file_dest)
    # Clean files based on file_info -- it goes through each file and
    # determines how to react to it
    # actions at this time are:
    # clean -- eliminate
    # archive -- move into the reports directory,444 and gz
    # leave alone -- leave as is (tentative debug option)
    
    for i in file_info.keys():
        if 'report' in i:
            if os.path.exists(file_info[i]):
                logging.info("Saving report file %s" % (i))
                os.rename(file_info[i], os.path.join(repdir, i))
            else:
                logging.info("Report %s doesn't exist, ignoring" % i)
        logging.info('Cleaning up %s file (name %s)...' % (i, file_info[i]))
        if not os.path.exists(file_info[i]):
            logging.warning('File %s does not exist, skipping' % (file_info[i]))
        else:
            skip_val = False            
            if 'skips' in config:
                if i in config['skips']:
                    logging.info('File %s skipped as per config' % (file_info[i]))
                    skip_val = True
            if not skip_val:
                    os.unlink(file_info[i])
                    logging.info('Deleted %s' % (file_info[i]))

def new_partition(config, file_info, day, dry_run):
    """
    new_partition
    This is a new, slower and more thorough partitioning command which breaks traffic into 5
    supercatgories: acknowledged, backscatter, short, scan and aberration. 
    
    The reason this command is slower is because it iteratively generates the sets it needs and relies on 
    having each set available.
    """
    #
    # Generate our known scanners; note that the filtering of knowns is done implicitly as part of
    # workrwf_qs
    # 
    cmd = '%s | rwfilter --input-pipe=stdin --proto=0-255 --pass=stdout | rwset stdin --sip=%s' % (
        workrwf_qs(config, day, dry_run),
        file_info['knownset'])
    util.warn_del(file_set['knownset'])
    util.run_silk_command(cmd)
    # Now drop backscatter; this is done by generating two subsidiary sets -- backscatter tcp and backscatter icmp
    cmd = "%s | rwfilter --input-pipe=stdin --not-sipset=%s --pass=stdout | rwuniq --field=1 --thresold=packets=1-3 --no-title | cut -d '|' -f 1 | rwsetbuild stdin %s" % (
        workrwf_qs(config, day, dry_run),
        file_info['knownset'],
        file_info['shortset'])
   # Drop backscatter first.
   
