#!/usr/bin/env python3
#
#
# Lansdowne annotation tool
#
# The annotation tool connects to a number of datafiles which contain
# annotations on specific data types -- port/proto combinations, ICMP type
# and codes, etc.
#
import csv, re, sys, copy
numseq = re.compile('^([0-9].+)-([0-9].+)$')

class annotator():
    """
    An annotator is a class of dictionary which takes two keys: a 
    protocol number and a port number; port numbers are expected to 
    be moot for the majority of protocols.  
    """
    def get_protostring(self, proto, port=None):
        result = "unk/unk" # No exceptions for missing data.
        if proto in self.data:
            # Get the protocol string
            if -1 in self.data[proto]:
                proto_str = self.data[proto][-1]
            else:
                proto_str = "unk"
            if port is not None: # We were passed a port
                if port != -1: 
                    if port in self.data[proto]:
                        port_str = "/" + " or ".join(list(self.data[proto][port]))
                    else:
                        port_str = "/unk"
                else:
                    port_str = ""
            result = "%s%s" % (proto_str, port_str)
        return result

    def store_protostring(self, proto, port, value):
        if not proto in self.data:
            self.data[proto] = {port:set([value])}
        else:
            if port > -1:
                # We're using -1 as a convention for the protocol name
                if port in self.data[proto]:
                    self.data[proto][port].add(value)
                else:
                    self.data[proto][port] = set([value])
            else:
                self.data[proto][port] = value # Overwrrite for official proto name
    def __repr__(self):
        result = ""
        for i in self.data.keys():
            dkeys = sorted(self.data[i].keys())
            for j in dkeys:
                if j == -1:
                    result += ("%3d %s\n" % (i, self.get_protostring(i,j)))
                else:
                    result += ("\t%5d %s\n" % (j, self.get_protostring(i,j)))
        return result
    
    def add(self, target):
        """
        In place update of data
        """
        for i in target.data.keys():
            if not i in self.data:
                self.data[i] = copy.copy(target.data[i])
            else:
                for j in target.data[i].keys():
                    if not j in self.data[i]:
                        self.data[i][j] = copy.copy(target.data[i][j])
                    else:
                        if j != -1:
                            self.data[i][j] = self.data[i][j].union(target.data[i][j])
        
    def __init__(self):
        self.data = {x:{} for x in range(0,256)}
            
def load_port_annotations_from_csv(pa_fn):
    """
    load_port_annotations_from_csv(pa_fn) -> dict
    This loads the port annotations for conversion and editing from a
    csv file
    The csv values are just "proto","port","desc"
    """
    result = annotator()
    with open(pa_fn, 'r') as pa_fh:
        reader = csv.DictReader(filter(lambda row:row[0] != '#', pa_fh))
        for row in reader:
            proto = int(row['proto'])
            if numseq.match(row['port']):
                start = int(numseq.match(row['port']).groups()[0])
                end = int(numseq.match(row['port']).groups()[1])
                for i in range(start, end + 1):
                    result.store_protostring(proto, i, row['desc'])
            else:
                port = int(row['port'])
                result.store_protostring(proto, port, row['desc'])
    return result

def save_port_annotations_as_csv(pa_fn,annotator):
    with open(pa_fn, 'w') as pa_fh:
        fieldnames = ['proto','port','desc']
        writer = csv.DictWriter(pa_fh, fieldnames = fieldnames)
        writer.writeheader()
        for i in annotator.data:
            for j in annotator.data[i]:
                # The perils of overloading; since I use a -1 convention
                # to indicate that a value is a protocol name, and otherwise
                # i havea  set, and since pyhthon strings are iterable, if I
                # don't recast the string to an iterator, it writes the string's
                # first character instead.
                if j == -1:
                    working = [annotator.data[i][j]]
                else:
                    working = annotator.data[i][j]
                for k in working:
                    x = {'proto':i,'port':j,'desc':k}
                    writer.writerow(x)
    
    


    
