#!/usr/bin/env bash
day=${1}
export PYTHONPATH=`pwd`
echo "Converting ${day}"
python3 convert.py -s ${day} -e ${day}
echo "Classifying ${day}"
python3 classify_v2.py -s ${day} -e ${day}
echo "Analyzing ${day}"
python3 analytics/baseline.py -s ${day} -e ${day} report
python3 analytics/ports.py -s ${day} -e ${day} report
python3 analytics/base_protocols.py -s ${day} -e ${day} report 
echo "Cleaning up ${day}" 
rm /nfs/lander/working/mcollins/data/silk/${day}/* 
echo "Took only photos, left only footprints"
