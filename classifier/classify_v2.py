#!/usr/bin/env python
#
# V2 Classifier, version 0.1-alpha
# this is an update in the classifier based upon the revisions I've posted in the paper.

import os
import sys
import glob
import argparse
import logging
import shutil

from lansdowne import util, date_glob
parser = util.init_parser(sys.argv[0], 'Classifier and reporting system for darkspace data')
parser.add_argument('--trace-th', type = int , default = 4, help = 'Threshold for trace vs. non-trace', dest = 'trace_th')
parser.add_argument('--short-th', type = int, default = 4, help = 'Threshold for short vs. long', dest = 'short_th')
parser.add_argument('--known-set', type = str, default = '', help = 'Path to known set', dest = 'known_set')
parser.add_argument('--target-set', type = str, default = '', help = 'Path to target set', dest = 'target_set')
arguments = parser.parse_args()

def local_cstate_from_args(arguments, cstate):
    """
    local_cstate_from_args(arguments: ArgumentParser, cstate: dict)
    Updates the cstate from command line arguments; this is focused
    on any local cstate information.  
    """
    if arguments.known_set != '':
        cstate['known_set'] = arguments.known_set
    else:
        cstate['known_set'] = None
    if arguments.target_set != '':
        cstate['target_set'] = arguments.target_set
    else:
        cstate['target_set'] = None
    cstate['trace_th'] = arguments.trace_th
    cstate['short_th'] = arguments.short_th    
    return cstate

def local_load_config(cstate):
    """
    global_load_config(cstate: dictionary)
    Loads and manages global (common to all scripts) configuration
    tasks.
    """
    # Now, we verify the minimal correctneess
    logging.info('Doing minimal correctness checks')
    if 'target_set' in cstate and cstate['target_set'] is not None:
        logging.info('Verifying target sets listed for analysis')
        # Verify that each target set is well formed
    else:
        logging.critical('No target set, terminating')
        sys.exit(-1)
    if not os.path.exists(cstate['data']):
        logging.critical('Data directory %s does not exist, terminating' % cstate['data'])
    else:
        logging.info('Data directory exists... continuing')
    # Check for known set -- delete if not present and note
    if 'known_set' in cstate:
        logging.info('Checking known set %s exists' % cstate['known_set'])
        if not os.path.exists(cstate['known_set']):
            logging.warning('known_set missing, treating as nonexistent')
            del cstate['known_set']
    # Check for target set -- crticial failure if absent
    if not os.path.exists(cstate['target_set']):
        logging.critical('target_set does notg exist, terminating')
        sys.exit(-1)
    if not 'work_dir' in cstate:
        cstate['work_dir'] = os.path.join(os.getcwd(),'working')
    cstate['work_prefix'] = '%s_%d' % (''.join(cstate['date'].split('/')), os.getpid())
    logging.info('Working directory set to %s, prefix to %s' % (cstate['work_dir'], cstate['work_prefix']))
    return cstate

#
# SECTION:  PARTITIONING AND ANALYSIS FUNCTIONALITY
# 
# This section covers the actual partitioning process
#
#
def generate_acksets(known_set, target_set, data_directory, date, working_directory, theta_trace, dry_run_flag):
    if dry_run_flag is True:
        logging.info('Generate acksets: dry run flag is high, so exiting')
        return
    else:
        logging.info('Generate acksets: starting operation')
    os.system('rwfilter --sipset=%s --dipset=%s --pass=stdout %s/*.gz | rwuniq --field=1 --values=distinct:dip,distinct:in  --no-title --threshold=distinct:in=0-%d | cut -d "|" -f 1 | rwsetbuild stdin ascan.set' % (known_set, target_set, data_directory, theta_trace))
    os.system('rwfilter --sipset=%s --dipset=%s --pass=stdout %s/*.gz | rwuniq --field=1 --values=distinct:dip,distinct:in  --no-title --threshold=distinct:in=%d-255 | cut -d "|" -f 1 | rwsetbuild stdin atrace.set' % (known_set, target_set, data_directory, theta_trace + 1))

def generate_shortset(known_set, target_set, data_directory, date, working_directory, theta_long, dry_run_flag):
    if dry_run_flag is True:
        logging.info('Generate shortset: dry run flag is high, so exiting')
        return
    else:
        logging.info('Generate shortset: starting operation')
    os.system('rwfilter --not-sipset=%s --dipset=%s --pass=stdout %s/*.gz | rwuniq --field=1 --values=records  --no-title --threshold=records=0-%d | cut -d "|" -f 1 | rwsetbuild stdin short.set' % (known_set, target_set, data_directory, theta_long))

def generate_traceset(known_set, target_set, data_directory, date, working_directory, theta_trace, dry_run_flag):
    if dry_run_flag is True:
        logging.info('Generate traceset: dry run flag is high, so exiting')
        return
    else:
        logging.info('Generate traceset: starting operation') 
    os.system('rwfilter --not-sipset=%s --dipset=%s --pass=stdout %s/*.gz | rwfilter --input-pipe=stdin --not-sipset=%s --pass=stdout | rwuniq --field=1 --values=distinct:in  --no-title --threshold=distinct:in=%d-255 | cut -d "|" -f 1 | rwsetbuild stdin trace.set' % (known_set, target_set, data_directory, 'short.set', theta_trace + 1))

def generate_partition_sets(known_set, target_set, data_directory, date, working_directory, dry_run_flag):
    if dry_run_flag is True:
        logging.info('Generate partition sets: dry run flag is high, so exiting')
        return
    else:
        logging.info('Generate partition sets: starting operation')
    os.system('rwfilter --not-sipset=%s --dipset=%s --pass=stdout %s/*.gz | rwfilter --input-pipe=stdin --not-sipset=%s --pass=stdout | rwfilter --input-pipe=stdin --not-sipset=%s --pass=stdout | rwuniq --field=sip,proto,flags --values=records,distinct:dport,distinct:dip,distinct:in  --no-title > report.txt' % (known_set, target_set, data_directory, 'short.set','trace.set'))

def process_partition_file(pfile, working_directory, dry_run_flag):
    """
    Processes the partition file and generates output that builds the ifnal query sets.
    """
    if dry_run_flag is True:
        logging.info('process_partition_file: dry run flag is high, so exiting')
        return
    else:
        logging.info('process_partition_file: starting operation')
    tcp_ips = set()
    icmp_ips = set()
    udp_ips = set()
    other_ips = set()
    with open(pfile, 'r') as pfh:
        for i in pfh.readlines():
            ip, proto = i[:-1].split('|')[0:2]
            ip = ip.strip()
            proto = int(proto)
            if proto == 1:
                icmp_ips.add(ip)
            elif proto == 6:
                tcp_ips.add(ip)
            elif proto == 17:
                udp_ips.add(ip)
            else:
                other_ips.add(ip)
    fset = [('tcp',tcp_ips), ('udp',udp_ips),('icmp',icmp_ips),('other',other_ips)]
    for root, data in fset:
        setf_from_set(data, root)
    return

def setf_from_set(sfdata, sfroot):
    print('Creating set %s' % sfroot)
    with open(sfroot + '.txt', 'w') as fh:
        for i in sfdata:
            fh.write("%s\n" % i)
    os.system('rwsetbuild %s %s' % (sfroot + '.txt', sfroot + '.set'))
    
def partition_tcp(tcp_set, target_set, data_directory, dry_run_flag):
    if dry_run_flag is True:
        logging.info('partition_tcp: dry run flag is high, so exiting')
        return
    else:
        logging.info('partition_tcp: starting operation')
    sctr_set = set()
    scan_set = set()
    os.system('rwfilter --proto=6 --pass=stdout --sipset=%s --dipset=%s %s/*.gz | rwuniq --no-title --field=sip,flags > tcp_report.txt' % (tcp_set, target_set, data_directory))
    with open('tcp_report.txt','r') as tfh:
        for i in tfh:
            ip,flags = i[:-1].split('|')[0:2]
            flags = set(flags.split())
            # We care only about S/A/F/R, if we see a SYN only, we mark it as a scan, otherwise we mark it as backscatter.  PUEC are treated as semantically null
            if 'A' in flags or 'F' in flags or 'R' in flags:
                sctr_set.add(ip)
            else:
                scan_set.add(ip)
    setf_from_set(sctr_set, 'tcpsctr')
    setf_from_set(scan_set, 'tcpscan')

def partition_icmp(icmp_set, target_set, data_directory, dry_run_flag):
    if dry_run_flag is True:
        logging.info('partition_icmp: dry run flag is high, so exiting')
        return
    else:
        logging.info('partition_icmp: starting operation')

    sctr_set = set()
    scan_set = set()
    os.system('rwfilter --proto=1 --pass=stdout --sipset=%s --dipset=%s %s/*.gz | rwuniq --no-title --field=sip,itype > icmp_report.txt' % (icmp_set, target_set, data_directory))
    with open('icmp_report.txt','r') as tfh:
        for i in tfh:
            ip,itype = i[:-1].split('|')[0:2]
            if int(itype) != 8:
                sctr_set.add(ip)
            else:
                scan_set.add(ip)
    setf_from_set(sctr_set, 'icmpsctr')
    setf_from_set(scan_set, 'icmpscan')

def dir_check(dirname, minfile, force):
    logging.debug('Checking %s for existence' % dirname)
    if os.path.exists(dirname):
        fc = len(glob.glob(os.path.join(dirname,'*')))
        if fc >= minfile:
            return True
        else:
            return False
    else:
        if force:
            os.makedirs(dirname)
            return True
        else:
            return False
        
if __name__ == '__main__':
    full_set_list = ['ascan', 'atrace', 'tcp', 'udp', 'short', 'trace', 'tcpsctr','tcpscan','icmpsctr','icmpscan','icmp','other', 'report', 'tcp_report','icmp_report']
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    cstate = local_load_config(cstate)
    print(cstate)
    dry_run_flag = cstate['is_dry_run']
    known_set = cstate['known_set']
    target_set = cstate['target_set']
    data_directory_root = cstate['data']
    report_directory_root = cstate['reports']
    working_directory = os.getcwd()
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    for date in iterator:
        print(date)
        data_directory = os.path.join(data_directory_root, date)
        report_directory = os.path.join(report_directory_root, date)
        if dir_check(data_directory, 2, False) is True:
            logging.info('Data directory %s exists' % data_directory)
        else:
            logging.error('Data directory %s does not exist, exiting' % data_directory)
            sys.exit(-1)
        if dir_check(report_directory, 1, True):
            logging.info('Report directory %s exists' % report_directory)        
        generate_acksets(known_set, target_set, data_directory, date, working_directory, 4, dry_run_flag)
        generate_shortset(known_set, target_set, data_directory, date, working_directory, 4, dry_run_flag)
        generate_traceset(known_set, target_set, data_directory, date, working_directory, 4, dry_run_flag)
        generate_partition_sets(known_set, target_set, data_directory, date, working_directory, dry_run_flag)
        process_partition_file('report.txt',working_directory, dry_run_flag)
        partition_tcp('tcp.set', target_set, data_directory, dry_run_flag)
        partition_icmp('icmp.set', target_set, data_directory, dry_run_flag)
        for i in full_set_list:
            fn = '%s.set' % i
            ft = '%s.txt' % i
            if os.path.exists(fn):
                shutil.move(fn, os.path.join(report_directory, fn))
            if os.path.exists(ft):
                os.unlink(ft)
