#!/usr/bin/env bash
#
# Dummy conversion script for testing purposes
# The dummy conversion script takes an input file and and output
# filename, then creates the output file with the input filename
# and the time of creation.

input_fn=${1}
output_fn=${2}

res="source does not exist"
if [ -e $input_fn ]
then
    res="source exists"
fi
printf "%s|%s|%s|%s\n" "${input_fn}" "${output_fn}" "${res}" "`date`" > ${output_fn}
  
