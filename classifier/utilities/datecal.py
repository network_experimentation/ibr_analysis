#!/usr/bin/env python3
#
#
# calendar.py
# calendar is a date tracking tool which can determine whether
# or not a particular date has had all of the relevant processing
# or reports done on it.
#
# inputs:
# Takes a date range (-s, -d) as normal, and a spec file (yml configuration)
# The spec file contains all the various checks it does to see if something is
# completed.  These are as regular expressions.
#
# outputs: json object showing the status
# this will be in the form date, then states for everything.
import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('rules', action = 'store', default = 'rules.yml', help ='rules file for checking the calendar')
parser.add_argument('category', action='append', nargs='*', help = 'Categories to check')
arguments = parser.parse_args()
print(arguments)


headers = ['date']

def local_cstate_from_args(args, cstate):
    csstate['targets'] = args.category[0]
    return cstate


def do_something(report_dir, data_dir, working_date, is_dry_run, dest_set):
    sys.stderr.write("Doing something on day %s , dry_run:%s\n" %(working_date, str(is_dry_run)))
    sys.stderr.flush()
    
    return results

if __name__ == '__main__':
    print("HI")
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    print(cstate)
                         
