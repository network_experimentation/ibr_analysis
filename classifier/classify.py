#!/usr/bin/env python3
#
#
import argparse
import logging
import sys
import tempfile
import time
import os
import yaml
import csv
import glob

from lansdowne import date_glob, actions, util, partition

# command line options
# initially, these are the date to analyze, the config file location, the log level, the logfile location

parser = util.init_parser(sys.argv[0], 'Classifier and reporting system for darkspace data')
parser.add_argument('-k', '--keep', type = str, action = 'append',
                    help = ('Defines files to keep; these will be dropped in the reports directory on completion'))
parser.add_argument('action', default = 'abort', help = 'Action the system takes.')

arguments = parser.parse_args()

epithets = [
    ('normset', 'Setfile of normal addresses'),
    ('allset','Setfile of all addresses'),
    ('shortset', 'Set of addresses which do too little to model alone'),
    ('shorttxt','Short textfile'),
    ('normtxt','Normal textfile'),
    ('shortrwf', 'RWF file of short activity'),
    ('shortph','Short placeholder file, can be rewritten with impunity'),
    ('knownset','Set of addresses which are known legit scanners'),
    ('knownrwf','RWF file of legit scanners'),
    ('bsset','Set of backscatter sources'),
    ('bsrwf','RWF file of backscatter traffic'),
    ('icmpbsset','set file of icmp backscatter'),
    ('intbsrwfa','First interim bs rwf'),
    ('intbsrwfb','Second interim bs rwf'),
    ('tcpscanrwf','tcp scan rwf'),
    ('tcpscanset','Scanner set'),
    ('unclassified','Unclassified'),
    ('workrwf', 'Working rwf'),
    ('tcpset','Set of all tcp ips'),
    ('oopset','Set of all unknown ips'),
    ('udpset','Set of all udp ips'),
    ('udpscanset','Set of all udp scanning ips'),
    ('icmpset','Set of all icmp ips'),
    ('icmpscanset','Set of all icmp scanning ips'),
    ('multiset','set of all multiprotocol ips'),
    ('holdset','Temporary holding set for operations'),
    ('holdtxt','Temporary holding textfile for operations'),
    ('holdrwf','Temporary holding rawfile for operations'),
    ('intersectreport','Intersection Report'),
]

mainkeepers = ['normset','shortset','bsset','allset','knownset','multiset','tcpscanset','udpscanset','icmpscanset','oopset']


def local_cstate_from_args(arguments, cstate):
    """
    local_cstate_from_args(arguments: ArgumentParser, cstate: dict)
    Updates the cstate from command line arguments; this is focused
    on any local cstate information.  
    """
    cstate['action'] = arguments.action
    cstate['keepers'] = mainkeepers
    if arguments.keep is not None:
        logging.info('File keep specified, files are : %s ' % (','.join(arguments.keep)))
        # The following step is an optimization and may be gratuitous
        epithet_names = set([n for n,l in epithets])
        for name in arguments.keep:
            if name in epithet_names:
                cstate['keepers'].append(name)
    return cstate



def local_load_config(cstate):
    """
    global_load_config(cstate: dictionary)
    Loads and manages global (common to all scripts) configuration
    tasks.
    """
    # Now, we verify the minimal correctneess
    logging.info('Doing minimal correctness checks')
    if 'target_sets' in cstate:
        logging.info('Verifying target sets listed for analysis')
        # Verify that each target set is well formed
        for i in cstate['target_sets']:
            if 'name' in i and 'path' in i:
                logging.info("Verifying set tagged '%s' at point %s" % (i['name'], i['path']))
                if os.path.exists(i['path']):
                    logging.info('File exists')
                else:
                    logging.critical('File does not exist, terminating')
                    sys.exit(-1)
            else:
                logging.critical('Missing critical variable (name or path) in value %s, terminating' % (str(i)))
                sys.exit(-1)                    
    if not os.path.exists(cstate['data']):
        logging.critical('Data directory %s does not exist, terminating' % cstate['data'])
    else:
        logging.info('Data directory exists... continuing')
    # Check for known set -- delete if not present and note
    if 'known_set' in cstate:
        logging.info('Checking known set %s exists' % cstate['known_set'])
        if not os.path.exists(cstate['known_set']):
            logging.warning('known_set missing, treating as nonexistent')
            del cstate['known_set']
    # Check for targert set -- crticial failure if absent
    if not os.path.exists(cstate['target_set']):
        logging.critical('target_set does notg exist, terminating')
        sys.exit(-1)
    if not 'work_dir' in cstate:
        cstate['work_dir'] = os.path.join(os.getcwd(),'working')
    cstate['work_prefix'] = '%s_%d' % (''.join(cstate['date'].split('/')), os.getpid())
    logging.info('Working directory set to %s, prefix to %s' % (cstate['work_dir'], cstate['work_prefix']))
    return cstate


if __name__ == '__main__':
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    cstate = local_load_config(cstate)
    # Set up the operations for the main loop
    # The partition operation work son raw data and starts from there for the iterator
    # The other operations work on report directories, and may want to check there
    # first.
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    for day in iterator:
        if cstate['is_dry_run']:
            logging.info("Dry Run")
        logging.info("Action: %s, date: %s" % (cstate['action'], day))
        if cstate['action'] == "partition":
            partition.gen_partition(cstate, day, cstate['is_dry_run'])
        elif cstate['action']  == "timeseries":
            actions.gen_timeseries(cstate, day, cstate['is_dry_run'])
        elif cstate['action'] == "popcount":
            actions.gen_pop_csv(cstate, day, cstate['is_dry_run'])
        elif cstate['action'] == 'kscan':
            actions.gen_topn_known_scan(cstate, 65535, day, cstate['is_dry_run'])
        elif cstate['action'] == 'tscan':
            actions.gen_topn_tcp_scan(cstate, 65535, day, cstate['is_dry_run'])
        elif cstate['action'] == 'uscan':
            actions.gen_topn_udp_scan(cstate, 65535, day, cstate['is_dry_run'])
        elif cstate['action'] == 'sscan':
            actions.gen_topn_short_scan(cstate, 65535, day, cstate['is_dry_run'])
        elif cstate['action'] == 'ddos':
            actions.gen_ddos_report(cstate, 250, day, cstate['is_dry_run'])
        elif cstate['action'] == 'status':
            actions.get_status(cstate, 250, day, cstate['is_dry_run'])
        else:
            sys.stdout.write("Aborting\n")
        
