#!/usr/bin/env python3
#
# convert.py
#
# This is the master conversion application, and replaces my older process_cut.bash script
# Improvements over process_cut
# Implemented in a more maintainable language
# Added date ranges
# Uses a unified config.yml with space for multiple convertors
# Commands:
# convert; exactly what it says on the tine
# clean: purge an existing directory
# check: status check on a directory 
import os
import sys
import glob
import argparse
import logging
import shutil

from lansdowne import util, date_glob
parser = util.init_parser(sys.argv[0], 'Data convertor for darkspace')
arguments = parser.parse_args()
def local_cstate_from_args(arguments, cstate):
    """
    local_cstate_from_args(arguments: ArgumentParser, cstate: dict)
    Updates the cstate from command line arguments; this is focused
    on any local cstate information.  
    Noop for convert.py
    """
    return cstate

def local_load_config(cstate):
    return cstate

if __name__ == '__main__':
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    cstate = local_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    for date in iterator:
        # Check to see if destination directory exists and if it doesn't, create it
        source_data = glob.glob(os.path.join(cstate['raw_data'], date, '*'))
        dd_n = os.path.join(cstate['data'], date)
        # Make the parent directories; the use of exist_ok knocks out the need for a specific directory check
        if not cstate['is_dry_run']:
            os.makedirs(dd_n, exist_ok = True)
        for sf_n in source_data:
            df_n = os.path.join(cstate['data'], date, os.path.basename(sf_n).split('.')[0] + '.rwf')
            convert = cstate['convert_cmd'].format(sf_n, df_n)
            compress = cstate['compress_cmd'].format(df_n)
            if cstate['is_dry_run']:
                print(convert)
                print(compress)
            else:
                os.system(convert)
                os.system(compress)
                
