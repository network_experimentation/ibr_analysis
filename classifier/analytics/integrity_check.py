#!/usr/bin/env python3
#
# template.py
#
# This is a template for a lansdowne date-based analysis script which
# will pop through all the directories, do all the relevant steps, &c.
import csv
import glob
import sys
import os
import time
from lansdowne import util, date_glob


classified_set_table = set(['ascan.set', 'atrace.set', 'icmpscan.set',
                            'icmpsctr.set', 'icmp.set', 'other.set',
                            'short.set',  'tcpscan.set', 'tcpsctr.set',
                            'tcp.set', 'trace.set', 'udp.set'])
# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['check', 'abort']
default_sr = '/nfs/lander/traces/ntelescope/isiw/'
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('--source-root', default = default_sr, type = str, help = 'The root directory for network telescope traces')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    cstate['source'] = args.source_root
    return cstate

def get_files_list(date, file_root):
    """
    get_files_list(date, origin_dir)
    date: day(string format)
    file_root: root directory; implicitly this is either the default_sr 
    or the data_root (for converted files) 
    Returns a list of filenames
    """ 
    result = glob.glob(os.path.join(file_root, date, '*'))
    return(result)

def check_converted_files(date, origin_root, converted_root):
    """
    check_converted_files
    Checks the status of converted files in converted_root/date/*.gz
    
    Returns
    Count of converted files with gz extensions
    Count of converted files w/o gz extensions
    Count of files in origin directory
    """
    origin_files = get_files_list(date, origin_root)
    converted_files = get_files_list(date, converted_root)
    corrected_converted_files = list(filter(lambda x:x.find('.gz') > -1, converted_files))
    result = (len(origin_files),
              len(converted_files), len(corrected_converted_files))
    return result

def check_classifier_generation(date, report_root, report_checklist):
    report_files = get_files_list(date, report_root)
    found_count = 0 
    for i in report_files:
        if os.path.basename(i) in report_checklist:
            found_count += 1
    return(found_count, len(report_files))
    
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            origin, converted, correct = check_converted_files(day, cstate['source'], cstate['data'])
            classified, total = check_classifier_generation(day, cstate['reports'], classified_set_table)
            print("%10s %3d %3d %3d %3d %3d" % (day, origin, converted, correct, classified, total))
