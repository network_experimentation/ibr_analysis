#!/usr/bin/env python3
#
# scan_report.py
#
# Given a list of scanners, break down all the activity.
# The report is going to be
# source ip, protocol, dport, flag, 
import csv
import sys
import os
import time
from lansdowne import util, date_glob

actions = ['report', 'abort']
parser = util.init_parser(sys.argv[0], 'Generates scan activity reports')
parser.add_argument('-p','--pure-thresh', type = int, default = 256, help =  'Threshold for differentiating pure scanning.  Inclusive')
parser.add_argument('-t','--dinky-thresh', type = int, default = 4, help = 'Threshold for differentiating dinky scanning.  Inclusive.')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def generate_base_datafile(data_directory, source_ip_set, dest_ip_set, dest_fn, is_dry_run):
    cmd_string = 'rwfilter --proto=17 --pass=stdout --sipset=%s --dipset=%s %s/*.gz | rwuniq --fields=sip,dport --no-title --values=distinct:dip,distinct:flags,rec > %s' % (
        source_ip_set, dest_ip_set, data_directory, dest_fn)
    if is_dry_run is True:
        print("Dry run, command string is %s" % cmd_string)
    else:
        os.system(cmd_string)
    return

def update_info(scan_rec, result_tbl, scan_type):
    result_tbl[scan_type][0] += 1
    result_tbl[scan_type][1] += scan_rec['recs']
    result_tbl[scan_type][2].add(scan_rec['sip'])
    result_tbl[scan_type][3].add(scan_rec['dport'])
    
def process_base_datafile(data_fn, is_dry_run):
    """
    process_base_datafile(data_fn) Converts the raw rwuniq input into
    a table of values that can be used for further processing.  These
    values include the 'dinkers' -- who are scanners who hit one
    destination, but do a lot of them.  I expect that dinkers and
    oncelers are related and I need terms that don't sound so stupid. 
    """
    # Okay, so I'm going to output a per-ip report and an event
    # report.  The per IP report is going to drop
    # IP, # of unique destinations, # of dinker scans, # of pure scans.
    result_tbl = {}
    pure_threshold = 256
    dink_threshold = 4
    counts = { 'pure': [0, 0, set([]), set([])],
               'dink': [0, 0, set([]), set([])],
               'median': [0, 0, set([]), set([])]
               }
    pure_count = 0
    dink_count = 0
    median_count = 0
    if is_dry_run is not True:
        with open(data_fn, 'r') as data_fh:
            for l in data_fh.readlines():
                sip, dport, dip_count, flag_count, rec_count = l[:-1].split('|')[0:5]
                sip = sip.strip()
                dport = int(dport)
                dip_count = int(dip_count)
                flag_count = int(flag_count)
                rec_count = int(rec_count)
                if rec_count <= dink_threshold:
                    code = 'dink'
                else:
                    if dip_count >= pure_threshold:
                        code = 'pure'
                    else:
                        code = 'median'
                update_info({'sip': sip, 'dport': dport, 'recs': rec_count}, counts, code)
                if not sip in result_tbl:
                    result_tbl[sip] = {dport: {'dip':dip_count, 'flags':flag_count, 'code': code, 'recs': rec_count}}
                else:
                    result_tbl[sip][dport] = {'dip':dip_count, 'flags':flag_count, 'code': code, 'recs': rec_count}
    else:
        print("Dry run, exiting")
    return result_tbl

def create_per_scan_report(data, output_fn, is_dry_run):
    if is_dry_run is False:
        with open(output_fn, 'w') as output_fh:
            dataline = ['sip','class','dport','dips','recs']
            writer = csv.DictWriter(output_fh, fieldnames = dataline)
            writer.writeheader()
            for i in data.keys():
                for j in data[i].keys():
                    dr = data[i][j]
                    if dr['code'] == 'pure':
                        # We dump pures, summarize the other two
                        datarow = {'sip': i, 'class': dr['code'], 'dport': j,
                                   'dips': dr['dip'], 'recs': dr['recs']}
                        writer.writerow(datarow)
    else:
        print("Dry run")
    
                        

def create_per_ip_report(data, output_fn, is_dry_run):
    if is_dry_run is False:
        with open(output_fn, 'w') as output_fh:
            dataline = ['ip', 'recs_d', 'recs_m', 'recs_p', 'scans_d', 'scans_m', 'scans_p','dports']
            writer = csv.DictWriter(output_fh, fieldnames = dataline)
            writer.writeheader()
            for i in data.keys():
                result_dict = {'ip': i, 'recs_d': 0, 'recs_m':0, 'recs_p':0, 'scans_d':0, 'scans_m': 0, 'scans_p': 0, 'dports':len(data[i].keys())}
                for j in data[i].keys():
                    suffix = data[i][j]['code'][0]
                    result_dict['recs_%s' % suffix] += data[i][j]['recs']
                    result_dict['scans_%s' % suffix] += 1
                writer.writerow(result_dict)
    else:
        print("Dry Run")
    return

        
    
def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    cstate['d_thresh'] = args.dinky_thresh
    cstate['p_thresh'] = args.pure_thresh
    return cstate

if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    print(cstate)
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            dn = (day.replace('/','_')) + '.txt'
            generate_base_datafile(
                os.path.join(cstate['data'], day), os.path.join(cstate['reports'],day,'udp.set'),
                cstate['target_set'], dn, cstate['is_dry_run'])
            result_tbl = process_base_datafile(dn, cstate['is_dry_run'])
            create_per_ip_report(result_tbl, os.path.join(cstate['reports'],day,'udp_scan_summary.csv'), cstate['is_dry_run'])
            create_per_scan_report(result_tbl, os.path.join(cstate['reports'],day,'udp_scan_events.csv'), cstate['is_dry_run'])
    
