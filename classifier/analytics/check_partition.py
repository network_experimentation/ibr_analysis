#!/usr/bin/env python3
#
#
# check_partition.py
# This function checks the integrity of partitions from the classifier; it does so
# by going into each classifier subdirectory and then dumping the sizes of each partition
# and the sizes of their candidate intersections.

import os
import sys
import csv
from lansdowne import util, date_glob

actions = ['intersect']
parser = util.init_parser(sys.argv[0], 'Verifies the integrity of the decision tree partitioning')
arguments = parser.parse_args()

def local_cstate_from_args(arguments, cstate):
    """
    local_cstate_from_args (arguments: parse_args(), cstate: dict)
    Updates the cstate (configuration state) struct with 
    data from local arguments. 
    """
    # Functino is a no-op in this code
    return cstate

def stubfn(cstate, day, stub):
    """
    stubfn(cstate: dict, day:string, stub: string)
    Returns the set filename from a stub.
    """
    
    working_dir = os.path.join(cstate['reports'], day,
                               'sets')
    fn = os.path.join(working_dir, '%sset.gz' % stub)
    return fn

def process_day(cstate, day):
    base_counts = ['all','bs','icmpscan','tcpscan','udpscan','short','norm','oop']
    intersections = [('norm','short'), ('norm','bs'),('short','bs')]
    base_intersects = []
    count_table = {}
    for i in base_counts:
        count_table[i] = util.get_set_count(stubfn(cstate, day, i))
    for j in intersections:
        key = '%sx%s' %(j[0],j[1])
        base_intersects.append(key) 
        count_table[key] = util.get_set_intersect(stubfn(cstate, day, j[0]),
                                                stubfn(cstate, day, j[1]))
    header = ['date'] + base_counts + base_intersects
    output = csv.DictWriter(sys.stdout, fieldnames = header)
    if not 'header_written' in cstate:
        output.writeheader()
        cstate['header_written'] = True
    count_table['date'] = day
    output.writerow(count_table)

fileset = {}
cstate = util.global_cstate_from_args(arguments)
cstate = local_cstate_from_args(arguments, cstate)
cstate = util.init(cstate)
cstate = util.global_load_config(cstate)
iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
for i in iterator:
    process_day(cstate, i)
