#!/usr/bin/env python3
import os
import sys
import time
import csv

import scipy.stats
from lansdowne import util, date_glob

actions = ['abort', 'swapstats', 'timeseries', 'ranks']
formats = ['csv','plaintext']
fmt_opts = ','.join(formats)
def_fmt = 'plaintext'
parser = util.init_parser(sys.argv[0], 'Compares topn lists')
parser.add_argument('-t','--threshold', type = int, default = '10')
parser.add_argument('-p','--protocol', type = int, default=6, help='Protocol to analyze, 6 for TCP, 1 for ICMP, 17 for UDP')
parser.add_argument('-o','--oformat', type = str, default='plaintext', help='Output format, defaults to %s out of %s' % (def_fmt, fmt_opts))
parser.add_argument('action', default='abort', help = 'Action the analysis script takes')
arguments = parser.parse_args()


def local_cstate_from_args(arguments, cstate):
    """
    local_cstate_from_args(arguments: parse_args(), cstate: dict)
    Updates the cstate (configuration state) struct with local 
    arguments
    """
    if arguments.protocol == 1:
        cstate['fname'] = 'icmpscan.csv'
    elif arguments.protocol == 6:
        cstate['fname'] = 'tcpscan.tgt_port.csv'
    elif arguments.protocol == 17:
        cstate['fname'] = 'udpscan.tgt_port.csv'
    else:
        sys.stderr.write("Error: no valid protocol chosen; terminating.\n")
        sys.exit(-1)
    if arguments.oformat == 'csv':
        cstate['format'] = 'csv'
    elif arguments.oformat == 'plaintext':
        cstate['format'] = 'plaintext'
    else:
        sys.stderr.wrote("Error, incorrect output format; terminating\n")
        sys.exit(-1)
    if arguments.action in actions:
        cstate['action'] = arguments.action
    else:
        sys.stderr.write("Error: action %s does not exist, choose from |%s|.  Terminating\n" % (arguments.action,','.join(actions)))
        sys.exit(-1)
    cstate['threshold']= arguments.threshold
    return cstate
                         
def process_file(cstate, day):
    working_file = os.path.join(cstate['reports'], day,
                                'reports', cstate['fname'])
    if not os.path.exists(working_file):
        sys.stderr.write("File for day %s doesn't exist, skipping\n" % day)
        sys.stderr.flush()
        return None
    result = [] 
    with open(working_file, 'r') as wf:
        reader = csv.DictReader(wf)
        for x in reader:
            result.append((x['port'],x['sources'], x['packets']))
        if len(result) == 0:
            return None
        return result 

def calc_l1(day1, day2):
    # day1 = today
    # day2 = yesterday
    total_l1 = 0
    voffset = 0 # We add one to this every time an element outside the topn is included
    for i in range(0, len(day1)):
        if day1[i] in day2:
            total_l1 += abs(day2.index(day1[i]) - i)
        else:
            voffset += 1
    virtual_len = len(day1) + voffset
    wc_v = ((virtual_len ** 2) + virtual_len)/2
    return float(total_l1)/float(wc_v)


def jaccard(day1, day2):
    s1 = set(day1)
    s2 = set(day2)
    n = len(s1.intersection(s2))
    d = len(s1.union(s2))
    result = float(n)/float(d)
    return result

def kendalltau(day1, day2):
    tau, p = scipy.stats.kendalltau(day1, day2)
    return tau

def rankplot(fileset, cstate):
    days = list(fileset.keys())
    # Step 1: get a list of all the elements
    tset = set()
    for i in days:
        tset = tset.union(set(fileset[i]))
    offset_rank = len(list(fileset.values())[0]) + 1
    portlist = sorted(list(tset), key = int)
    # l is now a sorted list of every service seen in the dataset
    port_values = ' '.join(list(map(lambda x:"%5d" % int(x), portlist)))
    if cstate['format'] == 'plaintext':
        print("%18s %s" % ("Date", port_values))
        for i in days:
            rlist = []
            for j in portlist:
                if j in fileset[i]:
                    rlist.append(fileset[i].index(j))
                else:
                    rlist.append(offset_rank)
            rstring = ' '.join(list(map(lambda x:"%5d" % int(x), rlist)))
            print("%18s %s" % (i, rstring))
    else:
        fieldnames = ['date'] + portlist
        writer = csv.DictWriter(sys.stdout, fieldnames = fieldnames)
        writer.writeheader()
        for i in days:
            rdict = {j:(fileset[i].index(j) if j in fileset[i] else offset_rank) for j in portlist}
            rdict['date'] = i
            writer.writerow(rdict)
    return
        
fileset = {}
cstate = util.global_cstate_from_args(arguments)
cstate = local_cstate_from_args(arguments, cstate)
cstate = util.init(cstate)
cstate = util.global_load_config(cstate)
iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])

if cstate['action'] == 'abort':
    sys.stderr.write("Aborting\n")
    sys.stderr.flush()
    sys.exit(-1)
else:
    for day in iterator:
        working_list = process_file(cstate, day)
        if working_list is not None:
            working_list.sort(key = lambda x:int(x[1]), reverse = True)
            fileset[day] = [a[0] for a in working_list[0:cstate['threshold']]]
    days = list(fileset.keys())
    
if cstate['action'] == 'swapstats':
    for i in range(1, len(days)):
        d1 = days[i]
        d2 = days[i - 1]
        print("%16s %8.4f %8.4f %8.4f" % (d1,
                                          kendalltau(fileset[d1], fileset[d2]),
                                          jaccard(fileset[d1], fileset[d2]),
                                          calc_l1(fileset[d1], fileset[d2])))
elif cstate['action'] == 'timeseries':
    for i in days:
        topn = ' '.join(map(lambda x: "%5d" % int(x), fileset[i]))
        print("%12s %s" %(i,topn))
elif cstate['action'] == 'ranks':
    rankplot(fileset, cstate)
