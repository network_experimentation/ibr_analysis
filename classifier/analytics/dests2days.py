#!/usr/bin/env python3
#
# dests2days.py
#
# Given a threshold for destinations and a date range, plot the number of distinct days that the
# addresses which count above a certain threshold appear in the set.
#
#
import sys
import os
import glob
import tempfile
from lansdowne import util, date_glob

DEFAULT_THRESH = 3 
parser = util.init_parser(sys.argv[0], 'Tools for manipulating onceler time series')
actions = ['abort', 'genset', 'countdays']
parser.add_argument('-b','--bag',type=str,help ='Bag prefix name, defaults to count', default='count')
parser.add_argument('-i','--inputset', type = str,
                    default = '', help = 'Initial filtering sets; only addresses in this set will be processed')
parser.add_argument('--over', action='store_true', help='Sets the data set to consist of ip addresses which appear equal to or over the threshold')
parser.add_argument('action',default='abort',help =' Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(arguments, cstate):
    cstate['ip_bag'] = arguments.bag + '.ip.bag'
    cstate['record_bag'] = arguments.bag + '.records.bag'
    cstate['day_bag'] = arguments.bag + '.days.bag'
    cstate['inputset'] = arguments.inputset
    cstate['action'] = arguments.action
    cstate['over'] = arguments.over
    return cstate

fileset = {}
cstate = util.global_cstate_from_args(arguments)
cstate = local_cstate_from_args(arguments, cstate)
cstate = util.init(cstate)
cstate = util.global_load_config(cstate)
iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])

def process_day(cstate, day):
    """
    process_day
    cstate -- standard cstate
    day -- day to process, in YYYY/MM/DD format (directory location)
    
    Master day processing command, which either creates a set of hosts active on a particular day, or creates a bag counting the number
    of ips observed in a provided set that were active that day.
    """
    working_directory = os.path.join(cstate['data'], day, '*gz')
    if cstate['action'] == 'genset':
        t_bagname = util.fetch_tf_name('/tmp','bag.dests2days.')
        buffer_bagname = util.fetch_tf_name('/tmp/', 'buf_bag.dests2days.')
        command = 'rwfilter --proto=6 --pass=stdout %s --flags-all=S/SAFPUR | rwbag --bag-file=sIPv4,records,%s' % (working_directory,t_bagname)
        util.run_silk_command(command)
        if not os.path.exists(cstate['record_bag']):
            os.rename(t_bagname, cstate['record_bag'])
        else:
            util.run_silk_command('rwbagtool --add %s %s > %s' % (t_bagname, cstate['record_bag'], buffer_bagname))
            os.rename(buffer_bagname,cstate['record_bag'])
        # Now we'll create the ip bag, which is done using rwuniq
        
        # I'm being parsimonious with the filenames here.  I use buffer as the temporary sum bag initially, then
        # I run again and use buffer for the cover set, then use t_bagname as the temporary sum bag for ips
        util.run_silk_command('rwfilter --proto=6 --pass=stdout %s --flags-all=S/SAFPUR | rwuniq --field=1 --value=dip-dist --no-ti | rwbagbuild --bag-input=stdin --key=sipv4 > %s' % (working_directory, t_bagname))
        if not os.path.exists(cstate['ip_bag']):
            os.rename(t_bagname, cstate['ip_bag'])
        else:
            util.run_silk_command('rwbagtool --add %s %s > %s' % (t_bagname, cstate['ip_bag'], buffer_bagname))
            os.rename(buffer_bagname, cstate['ip_bag'])
            os.unlink(t_bagname)
    elif cstate['action'] == 'countdays':
        t_bagname = util.fetch_tf_name('/tmp','bag.dests2days.')
        buffer_bagname = util.fetch_tf_name('/tmp/', 'buf_bag.dests2days.')
        command = 'rwfilter --sipset=%s --proto=6 --pass=stdout %s | rwset --sip-file=stdout | rwbagbuild --set-input=stdin --key=sipv4 --default-count=1 > %s' % (cstate['inputset'], working_directory, t_bagname)
        util.run_silk_command(command)
        if not os.path.exists(cstate['day_bag']):
            os.rename(t_bagname, cstate['day_bag'])
        else:
            util.run_silk_command('rwbagtool --add %s %s > %s' % (t_bagname, cstate['day_bag'], buffer_bagname))
            os.rename(buffer_bagname,cstate['day_bag'])
            os.unlink(t_bagname)

if __name__ == '__main__':
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    else:
        for day in iterator:
            print(day)
            working_list = process_day(cstate, day)
            # if working_list is not None:
            #     working_list.sort(key = lambda x:int(x[1]), reverse = True)
            #     fileset[day] = [a[0] for a in working_list[0:cstate['threshold']]]
            #days = list(fileset.keys())

    
