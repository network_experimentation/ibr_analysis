#!/usr/bin/env python3
import os
import sys
import time
import csv

import scipy.stats
from lansdowne import util, date_glob

actions = ['abort', 'swapstats', 'timeseries', 'ranks']
formats = ['csv','plaintext']
fmt_opts = ','.join(formats)
def_fmt = 'plaintext'
parser = util.init_parser(sys.argv[0], 'Compares topn lists')
parser.add_argument('-o','--oformat', type = str, default='plaintext', help='Output format, defaults to %s out of %s' % (def_fmt, fmt_opts))
parser.add_argument('action', default='abort', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(arguments, cstate):
    cstate['action'] = arguments.action
    return cstate

def process_day(cstate, day):
    working_directory = os.path.join(cstate['reports'],day,'reports')
    result = {}
    scanfiles = {
        'known':'knownscan.tgt_port.csv',
        'short':'shortscan.tgt_port.csv',
        'scan':'tcpscan.tgt_port.csv'}
    for i in scanfiles:
        result[i] = load_data_from_csv(os.path.join(working_directory, scanfiles[i]))
    osets = {}
    for i in result:
        osets[i]= list(map(lambda x:x['port'], list(filter(lambda x:int(x['proto']) == 6, result[i]))))
    for n in [10,20,50,100]:
        rsets = {}
        for i in osets:
            rsets[i] = set(osets[i][0:n])
        dkeys = {}
        for i in rsets:
            for j in rsets:
                if i!=j:
                    if not (i,j) in dkeys:
                        print(day,n,i,j,len(rsets[i].intersection(rsets[j])))
                        dkeys[(i,j)] = 1
                        dkeys[(j,i)] = 1
    return result
    
def load_data_from_csv(fn, lim = 1000):
    result = []
    with open(fn, 'r') as df:
        r = csv.DictReader(df)
        for i in r:
            if not 'proto' in i:
                i['proto'] = '6'
            result.append(i)
    result.sort(key=lambda x:int(x['sources']), reverse=True)
    return result[:lim]
            

fileset = {}
cstate = util.global_cstate_from_args(arguments)
cstate = local_cstate_from_args(arguments, cstate)
cstate = util.init(cstate)
cstate = util.global_load_config(cstate)
iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])

if cstate['action'] == 'abort':
    sys.stderr.write("Aborting\n")
    sys.stderr.flush()
    sys.exit(-1)
else:
    for day in iterator:
        working_list = process_day(cstate, day)
        # if working_list is not None:
        #     working_list.sort(key = lambda x:int(x[1]), reverse = True)
        #     fileset[day] = [a[0] for a in working_list[0:cstate['threshold']]]
    days = list(fileset.keys())
    
