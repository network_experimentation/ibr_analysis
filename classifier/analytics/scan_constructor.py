#!/usr/bin/env python3
#
# scan_constructor.py
#
# This is an analytic script which takes a sequence of flow records and merges them into scan events; a scan event is a set of
# records from the same source ip to multiple destination ips within a limited timeout.
#
#
import sys, os, time, math

def weigh(dips, pkts, netsize):
    result = (math.log(1+dips)/math.log(netsize)) *((math.log(1 + (pkts/netsize))/math.log(2)))
    return result

if __name__ == '__main__':
    timeout = 900
    sipset=sys.argv[1]
    workdir = sys.argv[2]
    table = {}
    data = os.popen('rwfilter --pass=stdout --sipset=%s --proto=6,17 --max-pass=100000 %s*rwf* | rwsort --field=1,5,4,9 | rwcut --no-title --field=1,2,4,5,9' %
                    (sipset, workdir), 'r')
    last_key = None
    lcount = 1 
    for i in data.readlines():
        if (lcount % 10000 == 0):
            sys.stderr.write('.')
            sys.stderr.flush()
        lcount +=1 
        sip,dip,dport,proto,evtime = i[:-1].split('|')[0:5]
        stime = time.mktime(time.strptime(evtime,'%Y/%m/%dT%H:%M:%S.%f'))
        key=('%s,%s,%s' % tuple(map(lambda x:x.strip(), [sip,dport,proto])))
        if not key in table:
            if last_key is not None:
                #Make sure we terminate the last iteration properly
                table[last_key][-1][0] = len(table[last_key][-1][0])
            table[key] = [[set([dip]), stime, stime,1]]
            last_key = key
        else:
            # Start with last element
            last_element = table[key][-1]
            if last_element[2] + timeout >= stime:
                # Update the last element
                last_element[2] = stime
                last_element[3] += 1
                last_element[0].add(dip)
            else:
                last_element[0] = len(last_element[0]) # Purely a memory-saving move
                table[key].append([set([dip]),stime,stime, 1])
    table[last_key][-1][0] = len(table[last_key][-1][0])
    dport_sum = {}
    for i in table.keys():
        for j in table[i]:
            dport = i.split(',')[1]
            netsize = 768
            pkts = j[3]
            dests = j[0]
            print("f:%40s %8.4f %10d %10d" % (i,weigh(dests, pkts, netsize),dests,pkts))
            if not dport in dport_sum:
                dport_sum[dport] = 0.0
            else:
                dport_sum[dport] += weigh(dests, pkts, netsize)
    s = sorted(dport_sum.keys())
    for k in s:
        print("%15s %f" % (k, dport_sum[k]))
            
                                          
                                    
             
        
