#!/usr/bin/env python3
#
# template.py
#
# This is a template for a lansdowne date-based analysis script which
# will pop through all the directories, do all the relevant steps, &c.
import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    
    return cstate


def do_something(report_dir, data_dir, working_date, is_dry_run, dest_set):
    print("Doing something on day %s , dry_run:%s" %(working_date, str(is_dry_run)))
    # Get TCP and ICMP Scattering events
    sctr_file = os.path.join(report_dir, working_date, 'short.set')
    data_glob = os.path.join(data_dir, working_date, '*')
    # Generate event output
    output_file = os.path.join(report_dir, working_date, 'short_summary.csv')
    cstr_summary = "rwfilter --sip=%s --dipset=%s %s --pass=stdout | rwuniq --field=proto,dport --values=sip-distinct,records,bytes,packets --column-sep=',' --no-columns > %s" % \
        (sctr_file, dest_set, data_glob,output_file)
    output_file = os.path.join(report_dir, working_date, 'short_summary.csv')
    
    if is_dry_run:
        print(cstr_summary)
    else:
        os.system(cstr_summary)
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)

    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    print(cstate)
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            do_something(cstate['reports'], cstate['data'], day, cstate['is_dry_run'], cstate['target_set'])

