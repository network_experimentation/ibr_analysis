#!/usr/bin/env python3
#
# udp_communicator.py
#
# Identifies and extracts the weird daily udp communication we've been dealing with for the past few weeks.
# This is done by doing the following;
# Extracting the top-n udp ports (this assumes that 

import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('action', default='report', help = 'Calculates basic statistics on the busiest udp ports to identify UDP communicator')
arguments = parser.parse_args()

def read_ports_data_file(df_n):
    rtable = []
    with open(df_n, 'r', newline = '' ) as df_h:
        reader = csv.DictReader(df_h)
        for row in reader:
            if row['protocol'] == '17':
                rtable.append((int(row['dPort']), int(row['sIP-Distinct'])))
    return(sorted(rtable, key=lambda x:x[1], reverse = True)[0:10])
    
def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    
    return cstate

def summarize(raw_data):
    """
    summarize
    Generates a summary a dataset
    6ns: min, q1, median, q3, max, mean, length
    """
    s = sorted(raw_data)
    sl = int(len(s)/4) # I'm being a bit sloppy here
    q0 = s[0]
    q1 = s[sl]
    q2 = s[sl*2]
    q3 = s[sl*3]
    q4 = s[-1]
    mean = sum(s) /len(s)
    return (q0,q1,q2,q3,q4,mean,len(s))

def generate_udp_stats(report_dir, silk_dir, working_date, known_set, target_set, is_dry_run, candidate_ports):
    """
    generate_udp_stats
    report_dir: directory holding reports
    silk_dir: directory holding raw data
    working_date: date to process in YYYY/MM/DD format, used to find the da ta file in reports and the raw data 
    known_set: set of acknowledged scanners
    target_set: target darkspace set
    is_dry_run: as usual
    candidate_ports: list of candidate udp ports in integer format, will be used as part of the filter
    """
    rt = {}
    cp_str = ','.join(map(lambda x:str(x[0]), candidate_ports))
    cmd = 'rwfilter --proto=17 --dport={2} --not-sipset={0} --dipset={1} --pass=stdout {3}/*.gz | rwcut --field=dport,bytes --no-ti'.format(
        known_set, target_set, cp_str, os.path.join(silk_dir, working_date))
    if is_dry_run:
        sys.stderr.write('Dry run, would execute {0}\n'.format(cmd))
        return
    # Read the query command and get a set of pipe-delimited dport, byte values
    # Convert values to a table with dports as keys and an unsorted list of bytes as values
    with os.popen(cmd, 'r') as query_h:
        for l in query_h.readlines():
            d, b = tuple(map(int, l.split('|')[0:2]))
            if not d in rt:
                rt[d] = [b]
            else:
                rt[d].append(b)
    # Create result table
    result = []
    for index, sources in candidate_ports:
        s = summarize(rt[index])
        result_row = {'dport': index, 'sips': sources, 'min': s[0], 'q1': s[1], 'med': s[2], 'q3': s[3], 'max': s[4], 'mean': "%.2f" % s[5], 'pkts': s[6]}
        result.append(result_row)
    return(result)
    # Now generate this as a csv table

def gen_csv_output(report_dir, working_date, is_dry_run, data_set):
    if is_dry_run:
        sys.stderr.write("CSV Output: dry run, exiting\n")
        return
    data = sorted(data_set, key = lambda x:x['sips'], reverse=True)
    fieldnames = ['dport','sips','min','q1','med','q3','max','mean','pkts']
    with open(os.path.join(report_dir, working_date, 'investigate_udp.csv'), 'w') as df_h:
        writer = csv.DictWriter(df_h, fieldnames = fieldnames)
        writer.writeheader()
        for row in data:
            writer.writerow(row) 
        
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            results = generate_udp_stats(cstate['reports'], cstate['data'], day, cstate['known_set'],
                                         cstate['target_set'], cstate['is_dry_run'],
                                         read_ports_data_file(os.path.join(cstate['reports'], day, 'allports.csv')))
            gen_csv_output(cstate['reports'], day, cstate['is_dry_run'], results)
            
