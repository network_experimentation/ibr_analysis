#!/usr/bin/env python3
#
# template.py
#
# This is a template for a lansdowne date-based analysis script which
# will pop through all the directories, do all the relevant steps, &c.
import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    
    return cstate


def do_something(report_dir, data_dir, working_date, is_dry_run, dest_set):
    print("Doing something on day %s , dry_run:%s" %(working_date, str(is_dry_run)))
    # Step 1 -- find the ack scan results set
    ascan_file = os.path.join(report_dir, working_date, 'ascan.set')
    ascan_pmap = os.path.join(data_dir, '..', 'sets', 'ack.pmap')
    data_glob = os.path.join(data_dir, working_date, '*')
    # Generate event output
    output_file = os.path.join(report_dir, working_date, 'ascan_events.csv')
    cstr_events = "rwfilter --sip=%s --dipset=%s %s --pass=stdout | rwsort --pmap-file=scnr:%s --field=src-scnr | rwuniq --pmap-file=scnr:%s --field=sip,src-scnr,dport --values=distinct:dip,records,bytes,stime-earliest,etime-latest --column-sep=',' --no-columns > %s" % \
        (ascan_file, dest_set, data_glob, ascan_pmap, ascan_pmap, output_file)
    output_file = os.path.join(report_dir, working_date, 'ascan_summary.csv')
    cstr_summary = "rwfilter --sip=%s --dipset=%s %s --pass=stdout | rwsort --pmap-file=scnr:%s --field=src-scnr | rwuniq --pmap-file=scnr:%s --field=src-scnr --values=distinct:sip,distinct:dip,distinct:dport,records,bytes --column-sep=',' --no-columns > %s" % \
        (ascan_file, dest_set, data_glob, ascan_pmap, ascan_pmap, output_file)
    if is_dry_run:
        print(cstr_events)
        print(cstr_summary)
    else:
        os.system(cstr_events)
        os.system(cstr_summary)
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    print(cstate)
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            do_something(cstate['reports'], cstate['data'], day, cstate['is_dry_run'], cstate['target_set'])
