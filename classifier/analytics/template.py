#!/usr/bin/env python3
#
# template.py
#
# This is a template for a lansdowne date-based analysis script which
# will pop through all the directories, do all the relevant steps, &c.
import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    cstate['d_thresh'] = args.dinky_thresh
    cstate['p_thresh'] = args.pure_thresh
    return cstate


def do_something(working_date, is_dry_run):
    print("Doing something on day %s , dry_run:%s" %(working_date, str(is_dry_run)))
    
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    print(cstate)
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            do_something(day, cstate['is_dry_run'])
