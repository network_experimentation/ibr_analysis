#!/usr/bin/env python3
from lansdowne import annotator
from lansdowne.util import colors, die
import argparse
import sys, os

ofilename='None'
actions = {'convert':
           'Convert a csv file into a binary file with a clean sweep',
           'display': 'Show contents of a file on screen',
           'add': 'Replace entries in a binary file with new data',
           'drop': 'Remove entries from a binary file',
           'update': 'Extend entries in a binary file',
           'stats': 'Get stats on a binary file',
           'query': 'Query the database for a specific port/proto combo',
           'exit': 'Exit'
    }
parser = argparse.ArgumentParser(prog=sys.argv[0], description =
                                 'Annotation support functions')
parser.add_argument('-i', '--infile', action = 'append',
                    help='File to add to input')
parser.add_argument('-o', '--outfile', action = 'store', default = ofilename,
                    help='Output file, defaults to %s' % ofilename)
parser.add_argument('-q','--query', 
                    help="Query to execute in the form port/proto",
                    action='append')
parser.add_argument('-e','--entry',
                    help="Entry to add in the form port/proto/value",
                    action="append")
parser.add_argument('action', type=str, choices = list(actions.keys()),
                    help='Action to execute', default = 'exit')
# annotation support tool; this manages edits and debugs of the annotation

arguments = parser.parse_args()

def load_source_file(source_fn):
    #
    #
    # Shoudl eventually move this over to annotator
    if not os.path.exists(source_fn):
        print("error: no file %s" % source_fn)
        sys.exit(-1)
    else:
        # Conditional tree for loading a file is as follows
        # if file ends in .csv -- load a .csv
        # if file does not end in .csv -- try loading binary
        # if file does not end in .csv and binary fails, try loading .csv
        # If all fail, give up.
        loaded = False
        result = None
        if os.path.splitext(source_fn)[1] != '.csv':
            try:
                result = annotator.load_port_annotations_from_binary(source_fn)
            except:
                sys.stderr.write(
                    colors['yellow'] +
                    "Failed to read binary file, trying .csv as backup" +
                    colors['reset'] + '\n')
        if not loaded:
                result = annotator.load_port_annotations_from_csv(source_fn)
        return result
    return

def merge_annotators(a1,a2):
    # Given two annotators, merge them together inplace with the first annotator
    # the return is largely cosmetic.
    a1.add(a2)
    # well, that was hard.  
    return(a1)

def add_entry(entry, annotator):
    print("Adding entry to annotator")
    return

def drop_entry(entry, annotator):
    print("Dropping entry from annotator")
    return

def update_entry(entry, annotator):
    proto, port, desc = entry.split('/')
    port = int(port)
    proto = int(proto)
    annotator.store_protostring(proto, port, desc)
    return annotator

def get_stats(annotator):
    print("Getting stats")
    return

def query(qspec, annotator):
    if qspec.find('/') == -1:
        proto = int(qspec)
        port = -1
    else:
        proto, port = tuple(list(map(int, qspec.split('/')[0:2])))
    sys.stdout.write("(%d/%d): %s\n" % (proto, port,
                                        annotator.get_protostring(proto, port)))
    return

if __name__ == '__main__':
    # Load up all data files
    annotators = []
    base = None
    for i in arguments.infile:
        annotators.append(load_source_file(i))
        if len(annotators) > 1:
            sys.stderr.write(colors['green'] + ("%d files found, merging\n" % 
                             len(annotators)))
        base = annotators[0]
        for i in annotators:
            base = merge_annotators(base, i)
    if arguments.action == 'exit':
        print("Exiting")
        sys.exit(-1)
    elif arguments.action == 'display':
        print(base)
    elif arguments.action == 'update':
        for i in arguments.entry:
            base = update_entry(i, base)
    elif arguments.action == 'stats':
        pass
    elif arguments.action == 'query':
        for i in arguments.query:
            query(i, annotators[0])
    else:
        sys.stderr.write("Exiting\n")
        sys.exit(-1)
    if arguments.outfile != "None":
        annotator.save_port_annotations_as_csv(arguments.outfile, base)
