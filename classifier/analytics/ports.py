#!/usr/bin/env python3
#
# ports.py
#
# On the list of "geez, why didn't i do this dumb stuff sooner" counts ports by
# different classes. 
import csv
import sys
import os
import time
from lansdowne import util, date_glob

# The util init_parser function generates an argparse parser which has the basic functions (state, end date, dry run) already
# specified.  After that you add your local options and call parse_args as normal
actions = ['abort']
parser = util.init_parser(sys.argv[0], 'Template')
parser.add_argument('action', default='report', help = 'Action the analysis script takes')
arguments = parser.parse_args()

def local_cstate_from_args(args, cstate):
    cstate['action'] = args.action
    
    return cstate


def do_something(report_dir, data_dir, working_date, is_dry_run, dest_set):
    print("Doing something on day %s , dry_run:%s" %(working_date, str(is_dry_run)))
    data_glob = os.path.join(data_dir, working_date, '*')
    dest_file = os.path.join(report_dir, working_date, 'allports.csv')
    # Note the use of the g here -- the reason for this is because -n will but 0 ahead of a header.
    # The other tradeoff here is between pulling the data into python and just processing in the shell.
    # -g is notoriously slow vs. -n, but I just add complexity by doing it in python.
    port_report = "rwfilter --dipset={0} {1} --pass=stdout | rwuniq --field=proto,dport --values=sip-distinct,records,bytes,packets,stime-earliest,etime-latest --column-sep=',' --no-columns | sort -t',' -k 1,2 -g > {2}".format(dest_set, data_glob, dest_file)
    if is_dry_run:
        print(port_report)
    else:
        os.system(port_report)
        
if __name__ == '__main__':
    fileset = {}
    cstate = util.global_cstate_from_args(arguments)
    cstate = local_cstate_from_args(arguments, cstate)
    cstate = util.init(cstate)
    cstate = util.global_load_config(cstate)
    iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])
    print(cstate)
    if cstate['action'] == 'abort':
        sys.stderr.write("Aborting\n")
        sys.stderr.flush()
        sys.exit(-1)
    elif cstate['action'] == 'report':
        for day in iterator:
            do_something(cstate['reports'], cstate['data'], day, cstate['is_dry_run'], cstate['target_set'])
