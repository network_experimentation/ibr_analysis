#!/usr/bin/env python3
#
# Use the ddos data to generate a timeseries
import os
import sys
import time
import yaml
import csv

from lansdowne import date_glob, util
# Borrowed from main code, turn into util later.

parser = util.init_parser(sys.argv[0], 'DDOS Report Analysis Tool')
parser.add_argument('-t', '--threshold', type=int, default=5000)
arguments = parser.parse_args()

def local_cstate_from_args(a, cstate):
    cstate['threshold'] = a.threshold
    return cstate

cstate = util.global_cstate_from_args(arguments)
cstate = local_cstate_from_args(arguments, cstate)
cstate = util.init(cstate)
cstate = util.global_load_config(cstate)

iterator = date_glob.day_range(cstate['start_date'], cstate['end_date'])

def run_command(cstate, day):
    working_file = os.path.join(cstate['reports'], day, 'reports', 'ddos.csv')
    if os.path.exists(working_file):
        with open(working_file,'r') as wf_h:
            fh = csv.DictReader(wf_h)
            total = 0
            total_pkts = 0 
            for row in fh:
                if int(row['packets']) >= cstate['threshold']:
                    total += 1
                    total_pkts += int(row['packets'])
            print("%s %d %d" % (day, total, total_pkts))
    else:
        sys.stderr.write("No file for day %s\n" % day)
for day in iterator:
    run_command(cstate, day)
    
