#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import pandas as pd
import sys
import argparse

MAX_ELTS_PER_PANEL = 6
data = pd.read_csv(sys.argv[1])
thresh = int(sys.argv[2])
# Identify the maximum value for the plot and then establish the plot limit
dheaders = list(data.columns.drop('date'))
dheaders.sort()
y_max = max(data[dheaders].apply(max))

# Now let's do a check for obscurity, we generate a count of 
counts = data[data[dheaders] < y_max].count()
sorted_counts = counts.sort_values()
elements = sorted_counts[sorted_counts > thresh].index
fig, axes = plt.subplots(1,1)


# Plotting proper
axes.yaxis.set_major_locator(MaxNLocator(10))
axes.xaxis.set_major_locator(MaxNLocator(6))
axes.plot(data['date'], data[elements], label=elements)
axes.set_xlabel('Date')
axes.set_ylabel('Rank')
plt.ylim([0, y_max - 1])
axes.invert_yaxis()
#for i in dheaders[1:]:
#    axes.plot(data[i], label=i)
plt.legend(bbox_to_anchor=(0,0.98,1.,.05),loc='lower left',mode='expand',ncol=10,markerscale=0.5,fontsize='xx-small')
plt.show()


